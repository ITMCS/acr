/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(function(){
	
	
   fetchAllACR(); 
});

function fnSetUntil(){
	var date = new Date($('#memDate').val());
	month = '' + (date.getMonth() + 1);
    day = '' + date.getDate();
	
	if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    

    $('#until').val([$('#year').val(), month, day].join('-'));
}

function fetchdetailsforcsoandrio(e,id){
	startLoading();
	$.get({
        url : path + '/acr/fetchByEmpCode',
        data : "empCode="+$(e).val(),
        success : function(response) {	
            hideLoading();
            if(response == 'emptyCode'){
                showError('<b>ERROR!</b><br> Please Enter Id');	
            }else if(response == 'notFound'){
                showError('<b>ERROR!</b><br> No Record found for the Provided Id');	
            }else{
                var json = JSON.parse(response);
                console.log(response);
                $('#'+id).html(json['engName']);
                
            }				
        }
    });
}

function previewHtml(option){
    startLoading();
    var acrresponse ;
    $.get({
        url : path + '/acr/fetchByEmpCode',
        data : "empCode="+$('#empCode').val(),
        success : function(response) {	
            hideLoading();
            if(response == 'emptyCode'){
                var newWindow = window.open();
                newWindow.document.write("<h1>Your Employee code is empty Please Enter</h1>");
            }else if(response == 'notFound'){
                var newWindow = window.open();
                newWindow.document.write("<h1>Your Employee code is incorrect Please Verify it</h1>");
            }else{
            	var json = JSON.parse(response);
            	$.get({
                    url : path + '/acr/fetchacrbyemployeepk/'+json['employeePk'],
                    async:false,
                    success : function(response) {
                    	console.log(response);
                    	acrresponse=response;
                    	
                    }
                });
            	
                if(option==='print'){
                    var htmlRes = htmlFormatter(response,acrresponse,option);
                    var newWin=window.open('','Print-Window');
                    newWin.document.open();
                    newWin.document.write(htmlRes);
                    newWin.document.close();         	
                }else{
                    var htmlRes = htmlFormatter(response,acrresponse,option);
                    var newWin=window.open();
                    newWin.document.open();
                    newWin.document.write(htmlRes);
                    newWin.document.close();  
                }
            }					
        }
    });
    
}

function htmlFormatter(response,acrresponse,option){
	var json = JSON.parse(response);
	var htmlResponse = '<!DOCTYPE html><html lang="en"><head><title>Annual Confidential Report</title><meta charset="utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css"><script src="https://code.jquery.com/jquery-3.3.1.js"></script><link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap.min.css"><style>.custom tr:nth-child(even) {  background-color: #dddddd;}</style></head><body><div class="jumbotron text-center"><h4>Employee General Information</h4><p>with ACR History</p></div><div class="container"><table class="table custom"><thead><tr><th colspan="4">Employee Personal Information</th></tr></thead><tbody><tr><td></td><td></td><td></td><td><img src="';
	var imagePath = json['imagePath'];
        var uploadPath = '/home/PIMSUploads/';
        var image = '/pimsresources/'+imagePath.replace(uploadPath,'');
        image=path+image;
	htmlResponse=htmlResponse+image+'" alt="not found" style="height:150px;width:150px;border: 2px solid #dddddd;"/></td></tr><tr><th>Name</th><td>'+json['engName']+'</td><th>Govt Id</th><td>';
	var govid=json['govId'];
	htmlResponse=htmlResponse+govid+'</td></tr><tr><th>Mother Name</th><td>';
	var mothername=json['engMotherName'];
	htmlResponse=htmlResponse+mothername+'</td><th>Sex</th><td>';
	var sex=json['gender'];
	htmlResponse=htmlResponse+sex+'</td></tr><tr><th>Father Name</th><td>';
	var fathername=json['engFatherName'];
	htmlResponse=htmlResponse+fathername+'</td><th>Date of Birth</th><td>';
	var birthdate=json['birthDate'];
	htmlResponse=htmlResponse+birthdate+'</td></tr><tr><th>Marital status</th><td>';
	var maritalstatus= json['matirialStatus'];
	htmlResponse=htmlResponse+maritalstatus+'</td><th>Home District</th><td>';
	var homedistrict=json['homeDistrict'];
	htmlResponse=htmlResponse+homedistrict+'</td></tr><tr><th>Religion</th><td>';
	var religion=json['religion'];
	htmlResponse=htmlResponse+religion+'</td><th>Rank</th><td>';
	var rank=json['rank'];
	htmlResponse=htmlResponse+rank+'</td></tr><tr><th>Designation</th><td>';
	var designation=json['designation'];
	htmlResponse=htmlResponse+designation+'</td><th>Location</th><td>';
	var location=json['location'];
	htmlResponse=htmlResponse+location+'</td></tr><tr><th>Oraganization</th><td>';
	var organization=json['organization'];
	htmlResponse=htmlResponse+organization+'</td><th>Order / join Type</th><td>';
	var OrderjoinType=json['joinType'];
	htmlResponse=htmlResponse+OrderjoinType+'</td></tr><tr><th>Order / join Date</th><td>';
	var Orderjoindate=json['joinDate'];
	htmlResponse=htmlResponse+Orderjoindate+'</td><th>Freedom Fighter</th><td>';
	var freedomfighter=json['freedomFighter'];
	htmlResponse=htmlResponse+Orderjoindate+'</td></tr><tr><th>PRL Date</th><td>';
	var prldate=json['prldate'];
	htmlResponse=htmlResponse+prldate+'</td><th>No Of spouse</th><td>';
	var nosupouse=json['nosupouse'];
	htmlResponse=htmlResponse+nosupouse+'</td></tr><tr><th>Batch</th><td>';
	var batch=json['batch'];
	htmlResponse=htmlResponse+batch+'</td><th>Confirmation Go Date</th><td>';
	var conformgodate=json['confirmationGoDate'];
	htmlResponse=htmlResponse+conformgodate+'</td></tr><tr><th>Confirmation Date</th><td>';
	var conformdate=json['confirmationDate'];
	htmlResponse=htmlResponse+conformdate+'</td><th>Expire Date</th><td>';
	var expiredate=json['expiryDate'];
	htmlResponse=htmlResponse+expiredate+'</td></tr><tr><th>Natinal Seniority</th><td>';
	var natinalseniority=json['natinalSeniority'];
	htmlResponse=htmlResponse+natinalseniority+'</td><th>Status</th><td>';
	var status=json['activeStatus'];
	htmlResponse=htmlResponse+status+'</td></tr><tr><th>Promote</th><td>';
	var promote=json['promote'];
	htmlResponse=htmlResponse+promote+'</td><th>Cadre</th><td>';
	var cadre=json['cadre'];
	htmlResponse=htmlResponse+cadre+'</td></tr><tr><th>Cadre Date</th><td>';
	var cadredate=json['cadreDate'];
	htmlResponse=htmlResponse+cadredate+'</td><th>Activity Status</th><td>';
	var activitystatus=json['activeStatus'];
	htmlResponse=htmlResponse+activitystatus+'</td></tr><tr><th>Placement</th><td>';
	var placement=json['placement'];
	htmlResponse=htmlResponse+placement+'</td><th>Code</th><td>';
	var code=json['code'];
	htmlResponse=htmlResponse+code+'</td></tr><tr><th>Additional Personal Info</th><td>';
	var addpersonalinfo=json['additionalPersonalInfo'];
	htmlResponse=htmlResponse+addpersonalinfo+'</td><th>Remarks</th><td>';
	var remarks=json['remarks'];
	htmlResponse=htmlResponse+remarks+'</td></tr></tbody></table><table id="example" class="table table-striped table-bordered" style="width:100%"><thead><tr><th colspan="7">Employee ACR History</th></tr><tr><th>RIO Id</th><th>ACR YEAR</th><th>From Date</th><th>ORU Submission Date</th><th>RIO Submission Date</th><th>CSO Submission Date</th><th>Remarks</th></tr></thead><tbody>';
	console.log(acrresponse);
	var acrJson = JSON.parse(acrresponse);
	var table='';
	$.each(acrJson,function(index,obj){
		table='<tr>'
		table=table+'<td>'+obj['oruId']+'</td>';
		table=table+'<td>'+obj['acrYear']+'</td>';
		table=table+'<td>'+obj['from']+'</td>';
		table=table+'<td>'+obj['oruSubDate']+'</td>';
		table=table+'<td>'+obj['oruSubMissionDate']+'</td>';
		table=table+'<td>'+obj['csoSubMissionDate']+'</td>';
		table=table+'<td>'+obj['remark']+'</td>';
		table=table+'</tr>';
	});
 
	htmlResponse=htmlResponse+table+'</tbody></table></div></body><script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script><script>'+(option==='print' ? "window.print();" : "")+'$(document).ready(function() {$("#example").DataTable();});</script></html>';
	return htmlResponse;
}


function fetchAllACR(){
    $.get({
        url : path + '/acr/fetchAll',
        data : "empCode="+$("#txtEmpCodeSearch").val(),
        success : function(response) {			
            if(response != ''){
                prepareGrid(response);
            }else{
                showError('<b>ERROR!</b><br> ACR records are not fetch sucessfully');
                hideLoading();
            }				
        }
    });
}

function setupForm(response){
    var arrresponse = response.split('#&#');
	var json = JSON.parse(arrresponse[0]);
	$('#acrPk').val(json['acrPk']);
	$('#empCode').val(json['pimsId']['govId']);
	$('#acrYear').val(json['acrYear']);
	$('#isAcrRequired').val(json['isAcrRequired']);
	$('#reason').val(json['reason']);
	$('#acrType').val(json['acrType']);
	$('#from').val(json['from']);
	$('#to').val(json['to']);
	$('#healthReport').val(json['healthReport']);
	$('#healthReportDate').val(json['healthReportDate']);
	$('#oruSubDate').val(json['oruSubDate']);
	$('#oruNo').val(json['oruNo']);
	$('#oruRemark').val(json['oruRemark']);
	$('#csoRemark').val(json['csoRemark']);
	$('#ifOruNegRemark').val(json['ifOruNegRemark']);
	$('#ifCsoNegRemark').val(json['ifCsoNegRemark']);
	$('#negremarksByoru').val(json['negremarksByoru']);
	$('#negremarksByCSO').val(json['negremarksByCSO']);
	$('#ifSugBYOru').val(json['ifSugBYOru']);
	$('#ifSugByCso').val(json['ifSugByCso']);
	$('#sugByOru').val(json['sugByOru']);
	$('#subByCso').val(json['subByCso']);
	$('#oruId').val(json['oruId']);
	$('#csoId').val(json['csoId']);
	$('#oruOfficeLoc').val(json['oruOfficeLoc']);
	$('#csoOfficeLoc').val(json['csoOfficeLoc']);
	$('#oruSignDate').val(json['oruSignDate']);
	$('#csoSignDate').val(json['csoSignDate']);
	$('#oruSubMissionDate').val(json['oruSubMissionDate']);
	$('#csoSubMissionDate').val(json['csoSubMissionDate']);
	$('#receiverId').val(json['receiverId']);
	$('#receiverDate').val(json['receiverDate']);
	$('#decisionForNegRemark').val(json['decisionForNegRemark']);
	$('#memNo').val(json['memNo']);
	$('#memDate').val(json['memDate']);
	$('#year').val(json['year']);
	$('#until').val(json['until']);
	$('#description').val(json['description']);
	$('#remark').val(json['remark']);
	$('#empname').html(json['pimsId']['engName']);
	$('#empid').html(json['pimsId']['govId']);
	var uploadPath = '/home/PIMSUploads/';
        var image = '/pimsresources/'+json['pimsId']['imagePath'].replace(uploadPath,'');
        image=path+image;
        console.log(image);
        $('#profileImg').attr('src',image);
	hideLoading();
	showHide();
}

function prepareGrid(leadData){			
    destroyTable('myTable');
    var headArr = ['#','ORU Id','ACR Year','From Date','ORU Submission Date','RIO Submission Date','CSO Submission Date', 'Remarks', 'Action' ];
    createHeader('theadAcr',headArr);
    $('#tbodyAcr').empty();
    if(checkValue(leadData) != '-'){
        leadData = JSON.parse(leadData);
        var count = 0;
        $.each(leadData, function( index, object ) {
            count++;
            var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyAcr'));
            $('<td data-sort="'+object['qualificationPk']+'">'+count+'</td>').appendTo(trTableBody);
            $('<td>'+object['oruId']+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['acrYear'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['from'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['oruSubDate'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['oruSubMissionDate'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['csoSubMissionDate'])+'</td>').appendTo(trTableBody);
            $('<td>'+checkValue(object['remark'])+'</td>').appendTo(trTableBody);
            $('<td><button class="btn btn-info" type="button" module="acr" value="'+object['acrPk']+'" onclick="fnEdit(this);showHide()" ><b><i class="fa fa-edit"></i></b></button>').appendTo(trTableBody);
        // &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="acr" value="'+object['acrPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>
        });
    }
    createTable('myTable');	
    hideLoading();
}

function fetchDetailsByEmpCode(){
    startLoading();
    $.get({
        url : path + '/acr/fetchByEmpCode',
        data : "empCode="+$('#empCode').val(),
        success : function(response) {	
            hideLoading();
            if(response == 'emptyCode'){
                showError('<b>ERROR!</b><br> Please Enter Employee Code');	
            }else if(response == 'notFound'){
                showError('<b>ERROR!</b><br> No Record found for the Provided Employee code');	
            }else{
                var json = JSON.parse(response);
                console.log(response);
                $('#empname').html(json['engName']);
                $('#empid').html(json['govId']);
                console.log(json['imagePath']);
                var uploadPath = '/home/PIMSUploads/';
                var image = '/pimsresources/'+json['imagePath'].replace(uploadPath,'');
                image=path+image;
                console.log(image);
                $('#profileImg').attr('src',image);
            }				
        }
    });
    
//    $.get({
//        url : path + '/acr/fetchDocumentByEmpCode',
//        data : "empCode="+$('#empCode').val(),
//        success : function(response) {	
//        	
//            if(response == 'emptyCode'){
//                showError('<b>ERROR!</b><br> Please Enter Employee Code');	
//            }else if(response == 'notFound'){
//                showError('<b>ERROR!</b><br> No Record found for the Provided Employee code');	
//            }else{
//                var json = JSON.parse(response);
//                var docJson = {};
//                var docArr=[];
//                $.each(json,function(index,obj){
//                    
//                   if(!docJson.hasOwnProperty(obj['documentType'])){
//                       docArr=[]
//                   } else{
//                      docArr= docJson[obj['documentType']];
//                   }
//                   docArr.push(obj);
//                   docJson[obj['documentType']]=docArr;
//                });
//                
//                $('#commonsectiontboby').html('');
//
//                console.log(docJson);
//                var tr = '';
//                $.each(docJson,function(index,obj){
//                    tr = tr + '<tr>';
//                    tr = tr + '<td>'+index+'</td>';
//                    tr = tr + '<td><a style="cursor:pointer;" onclick="fnOpenDocumentModel("'+index+'")"><u>Tag</u></a></td>';
//                    tr = tr + '/<tr>';
//                });
//                $('#commonsectiontboby').html(tr);
//
//            }				
//        }
//    });
}




