/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pims.acr;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pims.common.dal.service.CommonDAO;
import com.pims.common.dal.service.OperationTypeEnum;
import com.pims.data.TblACR;
import com.pims.data.TblACRDocumentMap;
import com.pims.data.TblEmployee;
import com.pims.data.TblUser;
import com.pims.management.generalinformation.EmployeeRepo;
import java.math.BigInteger;

/**
 *
 * @author ITMCS-PC
 */
@Repository
public class ACRRepoImpl implements ACRRepo{

    @Autowired
    CommonDAO commonDAO;

    @Autowired
    EmployeeRepo empRepo;
    
    
    @Override
    public TblACR Acrsavedatarepo(TblACR tblACR) {
        commonDAO.saveOrUpdate(tblACR);
        return tblACR;
    }

    @Override
    public List<TblACR> FetchAllDataByUserIdrepo(TblUser tblUser,String code) {
        if(code!=null && !code.trim().isEmpty()){
            TblEmployee tblEmployee = empRepo.getemployeedatabygovmentidrepo(code);
            return commonDAO.findEntity(TblACR.class,"createdBy.userPk",OperationTypeEnum.EQ,tblUser.getUserPk(),"pimsId.employeePk",OperationTypeEnum.EQ,tblEmployee.getEmployeePk());
        }else{
            return commonDAO.findEntity(TblACR.class,"createdBy.userPk",OperationTypeEnum.EQ,tblUser.getUserPk());
        }
    }

    @Override
    public void deleteById(BigInteger id) {
        TblACR tblAcr = new TblACR();
        tblAcr.setAcrPk(id);
        commonDAO.delete(tblAcr);
    }

    @Override
    public TblACR fetchById(BigInteger id) {
        List<TblACR> lstAcr = commonDAO.findEntity(TblACR.class,"acrPk",OperationTypeEnum.EQ,id);
        if(lstAcr!=null && !lstAcr.isEmpty()){
            return lstAcr.get(0);
        }
        return null;
    }

    @Override
    public List<TblACR> fetchacrbyemployeepkrepo(BigInteger employeepk) {		
        return commonDAO.findEntity(TblACR.class, "pimsId.employeePk",OperationTypeEnum.EQ,employeepk);
    }

    @Override
    public void saveAcrDocMap(List<TblACRDocumentMap> lstACRDocumentMap) {
       commonDAO.saveOrUpdateAll(lstACRDocumentMap);
    }

    @Override
    public void deleteDocbyAcr(TblACR tblAcr) {
        List<TblACRDocumentMap> list = commonDAO.findEntity(TblACRDocumentMap.class, "acrPk.acrPk",OperationTypeEnum.EQ,tblAcr.getAcrPk());
        for(TblACRDocumentMap tblAcrmap : list){
            commonDAO.delete(tblAcrmap);
        }
    }

    @Override
    public List<TblACRDocumentMap> fetchDocByAcr(TblACR tblAcr) {
        return commonDAO.findEntity(TblACRDocumentMap.class, "acrPk.acrPk",OperationTypeEnum.EQ,tblAcr.getAcrPk());
    }
    
}
