/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pims.acr;

import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblACR;
import com.pims.data.TblACRDocumentMap;
import com.pims.data.TblUser;
import java.math.BigInteger;

/**
 *
 * @author ITMCS-PC
 */
@Service
public interface ACRRepo {
	
	public TblACR Acrsavedatarepo(TblACR tblACR);
	public List<TblACR> FetchAllDataByUserIdrepo(TblUser tblUser,String empCode);
        public void deleteById(BigInteger id);
        public TblACR fetchById(BigInteger id);
        public List<TblACR> fetchacrbyemployeepkrepo(BigInteger employeepk);

        public void saveAcrDocMap(List<TblACRDocumentMap> lstACRDocumentMap);
        public void deleteDocbyAcr(TblACR tblAcr);
        
        public List<TblACRDocumentMap> fetchDocByAcr(TblACR tblAcr);
}
