package com.pims.bean;

import com.pims.data.TblUser;

public class SessionBean {
	TblUser user;

	public TblUser getUser() {
		return user;
	}

	public void setUser(TblUser user) {
		this.user = user;
	}
}
