package com.pims.mastertable.lookup;


import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.pims.data.TblLookUp;

@Repository
public interface LookUpRepo {

    TblLookUp addLookUp(TblLookUp tblLookUp);

    TblLookUp fetchLookUpbyLookUpId(BigInteger lookUpID);

    List<TblLookUp> fetchAllLookUp();

    boolean deleteLookUp(TblLookUp tblLookUp);

    List<TblLookUp> fetchLookUpByName(String name);
    
    List<TblLookUp> fetchLookUpByName(List<String> names);
}
