package com.pims.mastertable.lookup;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pims.bean.SessionBean;
import com.pims.common.utility.service.CommonUtility;
import com.pims.data.TblLookUp;
import com.pims.data.TblUser;
import com.pims.management.user.UserService;

@Controller
@RequestMapping("lookup")
public class LookUpController {

	@Autowired
	LookUpService lookUpService;
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addLookUp(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelLookUp", new TblLookUp());
			model.addAttribute("lstLookUp", CommonUtility.convertToJsonString(lookUpService.fetchAllLookUp()));
			
			return "com.pims.lookup";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveLookUp(@ModelAttribute("modelLookUp") TblLookUp tblLookUp, HttpServletRequest request, Model model) {
		try {
			if(tblLookUp!=null) {
				HttpSession httpSession = request.getSession();
				SessionBean sessionBean = (SessionBean) httpSession.getAttribute("AUTHSESSION");
				
				tblLookUp.setCreatedBy(sessionBean.getUser());
				lookUpService.addLookUp(tblLookUp);
				return "SuccessFullySaved";
			}else {
				return "EmptyForm";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String saveLookUp(HttpServletRequest request, Model model) {
		try {
			String lstLookUp = CommonUtility.convertToJsonString(lookUpService.fetchAllLookUp());
			return lstLookUp;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{lookUpId}", method = RequestMethod.GET)
	public String deleteLookUp(@PathVariable("lookUpId")BigInteger lookUpId,HttpServletRequest request, Model model) {
		try {
			String resposneString =lookUpService.deleteLookUp(lookUpId);
			return resposneString;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/edit/{lookUpId}", method = RequestMethod.GET)
	public String edit(@PathVariable("lookUpId")BigInteger lookUpId,HttpServletRequest request, Model model) {
		try {
			TblLookUp tblLookUp =lookUpService.fetchLookUpbyId(lookUpId);
			return CommonUtility.convertToJsonString(tblLookUp);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	
	
}
