package com.pims.mastertable.lookup;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblLookUp;

@Service
public interface LookUpService {

    TblLookUp addLookUp(TblLookUp lblLookUp);

    List<TblLookUp> fetchAllLookUp();

    String deleteLookUp(BigInteger lookUpId);

    TblLookUp fetchLookUpbyId(BigInteger lookUpId);

    List<TblLookUp> fetchLookUpByKeword(String keyword);
    
    List<TblLookUp> fetchLookUpByKeyword(List<String> keywords);
}
