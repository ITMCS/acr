package com.pims.common.utility.service;



import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pims.data.TblUser;

@Controller
public class CommonController {	
	
	@RequestMapping("/index")
    public String index(Model model,HttpServletRequest request){	
		
		return "home.dashboard";
    }
	
	
		
	@RequestMapping("/login")
	public String login(Model model, HttpServletRequest request) {		
		if (null != request.getSession().getAttribute("AUTHSESSION")) {
			HttpSession session = request.getSession();
			session.removeAttribute("AUTHSESSION");
			session.invalidate();
		}
		model.addAttribute("userModel", new TblUser());
		return "beforeAuth/login";
	}	
	
	@RequestMapping("/logout")
	public String logout(Model model, HttpServletRequest request) {
		HttpSession session = request.getSession();
		session.removeAttribute("AUTHSESSION");
		session.invalidate();
		return "beforeAuth/login";
	}
	
	@RequestMapping("/signup")
    public String signup(Model model){		
		model.addAttribute("userModel",new TblUser());
        return "beforeAuth/signUp";
    }
	
}
