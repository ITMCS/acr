package com.pims.common.utility.service;

import java.io.File;
import java.io.IOException;
import java.math.BigInteger;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.multipart.MultipartFile;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.pims.bean.SessionBean;
import com.pims.data.TblLanguage;
import com.pims.data.TblMenu;
import com.pims.data.TblUser;
import com.pims.management.menu.MenuService;
import com.pims.management.menu.MenuServiceImpl;

public class CommonUtility {
	
	
	
	private static String UPLOADED_FOLDER = "/home/PIMSUploads/";
	
	public static String convertToJsonString(Object obj) {
		Gson gson = new GsonBuilder().disableHtmlEscaping().create();		
		return gson.toJson(obj);
	}
	
	public static void setToSession(HttpServletRequest request,String key,Object value) {
		HttpSession httpSession = request.getSession();
		httpSession.setAttribute(key, value);
		
	}
	
	public static String buildLanguageValJSON(List<TblLanguage> lstLanguageElement) {
		Map<String,Object> mapMainKey = new HashMap();
		for( TblLanguage tblLanguage: lstLanguageElement) {
			Map<String,Object> dataSet = new HashMap();
			dataSet.put("en", tblLanguage.getEnglishName());
			dataSet.put("bg", tblLanguage.getBengaliName());
			mapMainKey.put(tblLanguage.getEnglishName(), dataSet);
		}
		return convertToJsonString(mapMainKey);
	}
	
	public static String uploadFile(MultipartFile file) throws IOException {		
		String fileUploadMsg = "FILEEMPTY";
		if (!file.isEmpty()) {
			 byte[] bytes = file.getBytes();
			 File fileDemo = new File(UPLOADED_FOLDER);
			 fileDemo.mkdirs();
			 Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());	         
	         Files.write(path, bytes);	
	         fileUploadMsg =UPLOADED_FOLDER + file.getOriginalFilename();
		}
		return fileUploadMsg;		
	}
	
	public static TblUser fetchUserFromSession(HttpServletRequest request) {
		
		HttpSession session = request.getSession();		
		SessionBean sessionBean = (SessionBean) session.getAttribute("AUTHSESSION");
		TblUser tblUser = sessionBean.getUser();		
		return tblUser;
	}
	
	
	public static String jsoMenuParentChildwise(List<TblMenu> lstMenu, List<BigInteger> lstChildList) {

		List<TblMenu> lstMenuParents = new ArrayList<TblMenu>();
		List<TblMenu> lstMenuChilds = new ArrayList<TblMenu>();
		List<BigInteger> parentIds = new ArrayList<BigInteger>();
		
		for (TblMenu tblMenu : lstMenu) {			
			if(lstChildList.contains(tblMenu.getMenuPk()) && tblMenu.getParent() == 1) {
				lstMenuChilds.add(tblMenu);
				if(!parentIds.contains(tblMenu.getParentSelfId())) {
					parentIds.add(tblMenu.getParentSelfId());
				}
			}else if(tblMenu.getChild() == 0 && tblMenu.getParent() == 0) {
				if(lstChildList.contains(tblMenu.getMenuPk()) && !lstMenuParents.contains(tblMenu)) {
					lstMenuParents.add(tblMenu);
					parentIds.add(tblMenu.getMenuPk());
				}
			}
			if(tblMenu.getChild() == 1) {
				if(!lstMenuParents.contains(tblMenu)) {
					lstMenuParents.add(tblMenu);
				}
			}
		}
		
		List<Map<String, Object>> lstMapParentChild = new ArrayList();
		for (TblMenu tblParents : lstMenuParents) {
			if (parentIds.contains(tblParents.getMenuPk())) {
				Map<String, Object> mapParentChild = new HashMap();
				mapParentChild.put("parent", tblParents);
				mapParentChild.put("child", "0");
				if (tblParents.getChild() == 1) {
					List<TblMenu> tempListMenu = new ArrayList();
					for (TblMenu tblChilds : lstMenuChilds) {
						if (tblParents.getMenuPk().equals(tblChilds.getParentSelfId())) {
							tempListMenu.add(tblChilds);
						}
					}
					mapParentChild.put("child", tempListMenu);
				}
				lstMapParentChild.add(mapParentChild);
			}
		}

		return convertToJsonString(lstMapParentChild);
	}
	
	
}
