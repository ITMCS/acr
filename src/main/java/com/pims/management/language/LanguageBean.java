package com.pims.management.language;


import com.pims.data.TblLanguage;

public class LanguageBean {
	
	TblLanguage[] lstLanguage;

	public TblLanguage[] getLstLanguage() {
		return lstLanguage;
	}

	public void setLstLanguage(TblLanguage[] lstLanguage) {
		this.lstLanguage = lstLanguage;
	}

}
