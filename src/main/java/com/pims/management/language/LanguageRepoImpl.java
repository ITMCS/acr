package com.pims.management.language;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pims.common.dal.service.CommonDAO;
import com.pims.common.dal.service.OperationTypeEnum;
import com.pims.data.TblLanguage;

@Repository
public class LanguageRepoImpl implements LanguageRepo{

	@Autowired
	CommonDAO commonDao;
	
	@Override
	public List<TblLanguage> fetchAllLanguageElements() {
		List<TblLanguage> lstlanguage  = commonDao.findEntity(TblLanguage.class);
		return lstlanguage;
	}

	@Override
	public String addAllLanguageElements(List<TblLanguage> tblLanguageElement) {	
		commonDao.saveOrUpdateAll(tblLanguageElement);		
		return "SUCCESSFULLYINSERT";
	}

	@Override
	public TblLanguage fetchLanguagebyID(BigInteger id) {
		List<TblLanguage> lstPostOffice  = commonDao.findEntity(TblLanguage.class, "languagePk",OperationTypeEnum.EQ ,id);
		TblLanguage dbObjTblLanguage = new TblLanguage();
		if(!lstPostOffice.isEmpty()) {
			dbObjTblLanguage =  lstPostOffice.get(0);
		}
		return dbObjTblLanguage;
	}

	@Override
	public boolean deleteLanguageEle(TblLanguage tblLanguage) {
		commonDao.delete(tblLanguage);
		return true;
	}

}
