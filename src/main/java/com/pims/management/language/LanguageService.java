package com.pims.management.language;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblLanguage;

@Service
public interface LanguageService {

	List<TblLanguage> fetchAllLanguageElements();
	
	String addLanguageElement(List<TblLanguage> tblLanguageElement);
	
	TblLanguage fetchById(BigInteger id);
	
	String deleteLanguageElement(BigInteger id);
}
