package com.pims.management.language;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pims.data.TblLanguage;

@Service
public class LanguageServiceImpl implements LanguageService{

	@Autowired
	LanguageRepo languageRepository;
	
	@Override
	public List<TblLanguage> fetchAllLanguageElements() {
		return languageRepository.fetchAllLanguageElements();
	}

	@Override
	public String addLanguageElement(List<TblLanguage> tblLanguageElement) {
		
		return languageRepository.addAllLanguageElements(tblLanguageElement);
	}

	@Override
	public TblLanguage fetchById(BigInteger id) {
		return languageRepository.fetchLanguagebyID(id);		
	}

	@Override
	public String deleteLanguageElement(BigInteger id) {
		String response = "INVALIDLANGUAGEELEID";
		TblLanguage tblLanguage = languageRepository.fetchLanguagebyID(id);
		if(tblLanguage != null) {
			if(languageRepository.deleteLanguageEle(tblLanguage)) {
				response = "DELETEDSUCCSSFULLY";
			}
		}
		return response;
		
	}
	
	
	
	
	

}
