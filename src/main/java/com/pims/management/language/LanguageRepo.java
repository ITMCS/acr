package com.pims.management.language;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.pims.data.TblLanguage;

@Repository
public interface LanguageRepo {

	List<TblLanguage> fetchAllLanguageElements();

	String addAllLanguageElements(List<TblLanguage> tblLanguageElement);

	TblLanguage fetchLanguagebyID(BigInteger id);

	boolean deleteLanguageEle(TblLanguage tblLanguage);
	
	

}
