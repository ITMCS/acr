package com.pims.management.generalinformation;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblEmployee;

@Service
public interface EmployeeService {

	TblEmployee addEmployee(TblEmployee lblEmployee);
	
	List<TblEmployee> fetchAllEmployee();
	
	String deleteEmployee(BigInteger employeeId);
	
	TblEmployee fetchEmployeebyId(BigInteger employeeId);

	String checkNewGovUniqueId(TblEmployee tblEmployee);
	
	public TblEmployee getemployeedatabygovmentidservice(String govmentid);
}
