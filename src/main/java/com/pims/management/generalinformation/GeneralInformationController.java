package com.pims.management.generalinformation;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.pims.bean.SessionBean;
import com.pims.common.utility.service.CommonUtility;
import com.pims.data.TblDocument;
import com.pims.data.TblEmployee;
import com.pims.data.TblUser;
import com.pims.management.user.UserService;


@Controller
@RequestMapping("generalinformation")
public class GeneralInformationController {

	
	@Autowired
	UserService userService;
	
	@Autowired
	EmployeeService employeeService;
	
	
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addRank(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelEmployee",new TblEmployee());
			return "com.pims.generalinformation";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveEmployee(@ModelAttribute("modelDocument") TblEmployee tblEmployee, HttpServletRequest request,
			Model model) {
		try {
			if (tblEmployee != null) {

				String chkUniqueCode = employeeService.checkNewGovUniqueId(tblEmployee);
				System.out.println("chkUniqueCode:>"+chkUniqueCode);
				if("UNIQUEGOVID".equalsIgnoreCase(chkUniqueCode)) {
					TblUser tblUser = CommonUtility.fetchUserFromSession(request);
					System.out.println("tblEmployee:>"+tblEmployee.getGovId());
				}
//				String fileUploadMsg = CommonUtility.uploadFile(tblDocument.getFileDoc().get(0));
//				if (!"FILEEMPTY".equalsIgnoreCase(fileUploadMsg)) {
//					tblDocument.setAttachment(fileUploadMsg);
//				}else{
//					if(tblDocument.getDocumentPk() != null && "FILEEMPTY".equalsIgnoreCase(fileUploadMsg)) {
//						tblDocument.setAttachment(tblDocument.getAttachment());
//					}
//				}
//				tblDocument.setCreatedBy(tblUser);
//				documentService.addDocument(tblDocument);
				
				return "redirect:/generalinformation/view";
			} else {
				return "EmptyForm";
			}
		} catch (Exception e) {
			e.printStackTrace();
			return e.getMessage();
		}
	}
}
