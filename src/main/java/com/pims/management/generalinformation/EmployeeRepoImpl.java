package com.pims.management.generalinformation;



import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pims.common.dal.service.CommonDAO;
import com.pims.common.dal.service.OperationTypeEnum;
import com.pims.data.TblEmployee;

@Repository
public class EmployeeRepoImpl implements EmployeeRepo{

	@Autowired
	CommonDAO commonDao;
	
	
	@Override
	public TblEmployee addEmployee(TblEmployee tblEmployee) {
		
		commonDao.saveOrUpdate(tblEmployee);
		return tblEmployee;
	}

	@Override
	public TblEmployee fetchEmployeebyEmployeeId(BigInteger employeeID) {		
		List<TblEmployee> lstEmployee  = commonDao.findEntity(TblEmployee.class, "employeePk",OperationTypeEnum.EQ ,employeeID);
		TblEmployee dbObjTblEmployee = new TblEmployee();
		if(!lstEmployee.isEmpty()) {
			dbObjTblEmployee =  lstEmployee.get(0);
		}
		return dbObjTblEmployee;
	}

	@Override
	public List<TblEmployee> fetchAllEmployee() {
		List<TblEmployee> lstEmployee  = commonDao.findEntity(TblEmployee.class);
		return lstEmployee;
	}

	@Override
	public boolean deleteEmployee(TblEmployee tblEmployee) {
		commonDao.delete(tblEmployee);
		return true;
	}

	@Override
	public boolean checkNewGovUniqueId(String govId) {
		
		List<TblEmployee> lstEmployee = commonDao.findEntity(TblEmployee.class,"govId",OperationTypeEnum.EQ,govId);
		System.out.println("lstEmployee:>"+lstEmployee.size());
		if(lstEmployee.isEmpty()) {
			return true;
		}else {
			return false;
		}		
	}

	@Override
	public TblEmployee getemployeedatabygovmentidrepo(String govmentid) {
		List<TblEmployee>  employeedatabygovmentid=commonDao.findEntity(TblEmployee.class,"govId",OperationTypeEnum.EQ,govmentid);
                if(employeedatabygovmentid!=null && !employeedatabygovmentid.isEmpty()){
                    return employeedatabygovmentid.get(0);
                }
		return null;
	}
	
	
	
}
