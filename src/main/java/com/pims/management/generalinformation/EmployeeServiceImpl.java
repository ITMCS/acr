package com.pims.management.generalinformation;

import java.math.BigInteger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pims.data.TblEmployee;

@Service
public class EmployeeServiceImpl implements EmployeeService{

	@Autowired
	EmployeeRepo employeeRepo;
	
	@Override
	public TblEmployee addEmployee(TblEmployee tblEmployee) {	
		//tblEmployee.setCreatedOn(new Date());
		//tblEmployee.setUpdatedOn(new Date());
		return employeeRepo.addEmployee(tblEmployee);
	}

	@Override
	public List<TblEmployee> fetchAllEmployee() {
		return employeeRepo.fetchAllEmployee();
	}
	
	@Override
	public String deleteEmployee(BigInteger employeeId) {
		String response = "INVALIDDOCUMENTID";
		TblEmployee tblEmployee = employeeRepo.fetchEmployeebyEmployeeId(employeeId);
		if(tblEmployee != null) {
			if(employeeRepo.deleteEmployee(tblEmployee)) {
				response = "DELETEDSUCCSSFULLY";
			}
		}
		return response;
	}

	@Override
	public TblEmployee fetchEmployeebyId(BigInteger employeeId) {
		return employeeRepo.fetchEmployeebyEmployeeId(employeeId);
	}

	@Override
	public String checkNewGovUniqueId(TblEmployee tblEmployee) {
		String response="DUPLICATEGOVID";
		boolean status = employeeRepo.checkNewGovUniqueId(tblEmployee.getGovId());
		if(status) {
			response="UNIQUEGOVID";
		}
		return response;
	}

	@Override
	public TblEmployee getemployeedatabygovmentidservice(String govmentid) {
		
		return employeeRepo.getemployeedatabygovmentidrepo(govmentid) ;
	}
	
}
