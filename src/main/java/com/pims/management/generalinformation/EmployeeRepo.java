package com.pims.management.generalinformation;


import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.pims.data.TblEmployee;

@Repository
public interface EmployeeRepo {

	TblEmployee addEmployee(TblEmployee tblEmployee);

	TblEmployee fetchEmployeebyEmployeeId(BigInteger employeeID);
	
	List<TblEmployee> fetchAllEmployee();
	
	boolean deleteEmployee(TblEmployee tblEmployee);

	boolean checkNewGovUniqueId(String govId);
	
	public TblEmployee getemployeedatabygovmentidrepo(String govmentid);
}
