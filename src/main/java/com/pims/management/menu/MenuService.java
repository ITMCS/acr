package com.pims.management.menu;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblMenu;

@Service
public interface MenuService {

	TblMenu addMenu(TblMenu tblMenu);
	
	List<TblMenu> fetchAllMenu();
	
	String deleteMenu(BigInteger menuId);
	
	TblMenu fetchMenubyId(BigInteger menuId);
	
	List<TblMenu> fetchAllChildMenu();
}
