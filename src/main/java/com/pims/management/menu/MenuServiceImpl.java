package com.pims.management.menu;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pims.data.TblMenu;

@Service
public class MenuServiceImpl implements MenuService{

	@Autowired
	MenuRepo menuRepo;
	
	@Override
	public TblMenu addMenu(TblMenu tblMenu) {	
		tblMenu.setCreatedOn(new Date());
		tblMenu.setUpdatedOn(new Date());
		return menuRepo.addMenu(tblMenu);
	}

	@Override
	public List<TblMenu> fetchAllMenu() {
		return menuRepo.fetchAllMenu();
	}
	
	@Override
	public List<TblMenu> fetchAllChildMenu() {
		return menuRepo.fetchAllChildMenu();
	}
	
	
	@Override
	public String deleteMenu(BigInteger MenuId) {
		String response = "INVALIDMENUID";
		TblMenu tblMenu = menuRepo.fetchMenubyMenuId(MenuId);
		if(tblMenu != null) {
			if(menuRepo.deleteMenu(tblMenu)) {
				response = "DELETEDSUCCSSFULLY";
			}
		}
		return response;
	}

	@Override
	public TblMenu fetchMenubyId(BigInteger MenuId) {
		return menuRepo.fetchMenubyMenuId(MenuId);
	}
	
}
