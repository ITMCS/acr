package com.pims.management.menu;


import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.pims.data.TblMenu;


@Repository
public interface MenuRepo {

	TblMenu addMenu(TblMenu tblMenu);

	TblMenu fetchMenubyMenuId(BigInteger MenuID);
	
	List<TblMenu> fetchAllMenu();
	
	boolean deleteMenu(TblMenu tblMenu);

	List<TblMenu> fetchAllChildMenu();
}
