package com.pims.management.menu;



import java.math.BigInteger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pims.common.dal.service.CommonDAO;
import com.pims.common.dal.service.OperationTypeEnum;
import com.pims.data.TblMenu;

@Repository
public class MenuRepoImpl implements MenuRepo{

	@Autowired
	CommonDAO commonDao;
	
	
	@Override
	public TblMenu addMenu(TblMenu tblMenu) {
		
		commonDao.saveOrUpdate(tblMenu);
		return tblMenu;
	}

	@Override
	public TblMenu fetchMenubyMenuId(BigInteger MenuID) {		
		List<TblMenu> lstMenu  = commonDao.findEntity(TblMenu.class, "menuPk",OperationTypeEnum.EQ ,MenuID);
		TblMenu dbObjTblMenu = new TblMenu();
		if(!lstMenu.isEmpty()) {
			dbObjTblMenu =  lstMenu.get(0);
		}
		return dbObjTblMenu;
	}

	@Override
	public List<TblMenu> fetchAllMenu() {
		List<TblMenu> lstMenu  = commonDao.findEntity(TblMenu.class);
		return lstMenu;
	}

	@Override
	public boolean deleteMenu(TblMenu tblMenu) {
		commonDao.delete(tblMenu);
		return true;
	}

	@Override
	public List<TblMenu> fetchAllChildMenu() {
		List<TblMenu> lstMenu  = commonDao.findEntity(TblMenu.class,"child",OperationTypeEnum.EQ ,0);
		return lstMenu;
	}
	
	
	
}
