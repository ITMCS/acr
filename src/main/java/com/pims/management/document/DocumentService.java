package com.pims.management.document;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblDocument;

@Service
public interface DocumentService {

	TblDocument addDocument(TblDocument lblDocument);
	
	List<TblDocument> fetchAllDocument();
	
	String deleteDocument(BigInteger documentId);
	
	TblDocument fetchDocumentbyId(BigInteger documentId);
	public List<TblDocument> fetchdataofdocumentbygovmentidservice(String govmentid);
}
