package com.pims.management.document;



import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pims.common.dal.service.CommonDAO;
import com.pims.common.dal.service.OperationTypeEnum;
import com.pims.data.TblDocument;

@Repository
public class DocumentRepoImpl implements DocumentRepo{

	@Autowired
	CommonDAO commonDao;
	
	
	@Override
	public TblDocument addDocument(TblDocument tblDocument) {
		
		commonDao.saveOrUpdate(tblDocument);
		return tblDocument;
	}

	@Override
	public TblDocument fetchDocumentbyDocumentId(BigInteger documentID) {		
		List<TblDocument> lstDocument  = commonDao.findEntity(TblDocument.class, "documentPk",OperationTypeEnum.EQ ,documentID);
		TblDocument dbObjTblDocument = new TblDocument();
		if(!lstDocument.isEmpty()) {
			dbObjTblDocument =  lstDocument.get(0);
		}
		return dbObjTblDocument;
	}

	@Override
	public List<TblDocument> fetchAllDocument() {
		List<TblDocument> lstDocument  = commonDao.findEntity(TblDocument.class);
		return lstDocument;
	}

	@Override
	public boolean deleteDocument(TblDocument tblDocument) {
		commonDao.delete(tblDocument);
		return true;
	}

	@Override
	public List<TblDocument> fetchdataofdocumentbygovmentidrepo(String govmentid) {
		List<TblDocument> dataofdocumentbygovmentid=commonDao.findEntity(TblDocument.class,"gId",OperationTypeEnum.EQ,govmentid);
		return dataofdocumentbygovmentid;
	}
	
	
	
}
