package com.pims.management.document;

import java.util.Date;
import java.math.BigInteger;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pims.data.TblDocument;

@Service
public class DocumentServiceImpl implements DocumentService{

	@Autowired
	DocumentRepo userRepo;
	
	@Override
	public TblDocument addDocument(TblDocument tblDocument) {	
		tblDocument.setCreatedOn(new Date());
		tblDocument.setUpdatedOn(new Date());
		return userRepo.addDocument(tblDocument);
	}

	@Override
	public List<TblDocument> fetchAllDocument() {
		return userRepo.fetchAllDocument();
	}
	
	@Override
	public String deleteDocument(BigInteger documentId) {
		String response = "INVALIDDOCUMENTID";
		TblDocument tblDocument = userRepo.fetchDocumentbyDocumentId(documentId);
		if(tblDocument != null) {
			if(userRepo.deleteDocument(tblDocument)) {
				response = "DELETEDSUCCSSFULLY";
			}
		}
		return response;
	}

	@Override
	public TblDocument fetchDocumentbyId(BigInteger documentId) {
		return userRepo.fetchDocumentbyDocumentId(documentId);
	}

	@Override
	public List<TblDocument> fetchdataofdocumentbygovmentidservice(String govmentid) {
		return userRepo.fetchdataofdocumentbygovmentidrepo(govmentid);
	}
	
}
