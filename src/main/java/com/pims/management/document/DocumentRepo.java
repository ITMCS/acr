package com.pims.management.document;


import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.pims.data.TblDocument;

@Repository
public interface DocumentRepo {

	TblDocument addDocument(TblDocument tblDocument);

	TblDocument fetchDocumentbyDocumentId(BigInteger documentID);
	
	List<TblDocument> fetchAllDocument();
	
	boolean deleteDocument(TblDocument tblDocument);
	
	public List<TblDocument> fetchdataofdocumentbygovmentidrepo(String govmentid);
}
