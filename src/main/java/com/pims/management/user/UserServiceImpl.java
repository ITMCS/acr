package com.pims.management.user;

import java.math.BigInteger;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.pims.common.utility.service.EncryptDecryptStringWithDES;
import com.pims.data.TblRole;
import com.pims.data.TblUser;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	UserRepo userRepo;
	
	@Override
	public String addUser(TblUser user) {	
		
		String response = "USEREXCIST";	
		if(user.getUserPk() != null) {
			user.setPassword(EncryptDecryptStringWithDES.encrypt(user.getPassword(),user.getUserName()));
			userRepo.addUser(user);
			response = "SuccessFullySaved";
		}else {
			TblUser dbObjTblUser = userRepo.fetchUserbyUserName(user.getUserName());
			if(dbObjTblUser == null) {
				user.setPassword(EncryptDecryptStringWithDES.encrypt(user.getPassword(),user.getUserName()));
				dbObjTblUser = userRepo.addUser(user);
				response = "SuccessFullySaved";
			}
		}
		return response;
	}

	@Override
	public TblUser authenticateUser(TblUser user) {
		TblUser dbObjTblUser = userRepo.fetchUserbyUserName(user.getUserName());
		if (null != dbObjTblUser) {
			String encryptedPassword = EncryptDecryptStringWithDES.encrypt(user.getPassword(),
					user.getUserName());
			return (encryptedPassword.equalsIgnoreCase(dbObjTblUser.getPassword())) ? dbObjTblUser : null;
		}
		else if("admin".equalsIgnoreCase(user.getUserName()) && "admin123".equalsIgnoreCase(user.getPassword())){
			return dbObjTblUser;
		}
		return null;
	}

	@Override
	public TblUser fetchUserByUsername(String username) {
		TblUser tblUser = userRepo.fetchUserbyUserName(username);
		return tblUser;
	}

	@Override
	public List<TblUser> fetchAllUser() {
		List<TblUser> lstUser = userRepo.fetchAllUser();
		return lstUser;
	}

	@Override
	public String deleteUserbyId(BigInteger userId) {
		String response = "INVALIDPOSTOFFICEID";
		TblUser tblUser = userRepo.fetchUserbyId(userId);
		if(tblUser != null) {
			if(userRepo.deleteUser(tblUser)) {
				response = "DELETEDSUCCSSFULLY";
			}
		}
		return response;
	}

	@Override
	public TblUser fetchUserbyId(BigInteger userId) {
		TblUser tblUser = userRepo.fetchUserbyId(userId);
		tblUser.setPassword(EncryptDecryptStringWithDES.decrypt(tblUser.getPassword(),tblUser.getUserName()));
		return tblUser;
	}

	@Override
	public List<TblRole> fetchAllRole() {
		List<TblRole> lstRole = userRepo.fetchAllRole();		
		return lstRole;
	}

	/**
	 * @return the userRepo
	 */
	public UserRepo getUserRepo() {
		return userRepo;
	}

	/**
	 * @param userRepo the userRepo to set
	 */
	public void setUserRepo(UserRepo userRepo) {
		this.userRepo = userRepo;
	}

	
	
	
}
