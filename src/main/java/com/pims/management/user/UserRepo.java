package com.pims.management.user;


import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Repository;

import com.pims.data.TblRole;
import com.pims.data.TblUser;

@Repository
public interface UserRepo {

	 TblUser addUser(TblUser user);
	 
	 String updateUser(TblUser user);

	 TblUser fetchUserbyUserName(String userName);

	List<TblUser> fetchAllUser();

	TblUser fetchUserbyId(BigInteger userId);

	boolean deleteUser(TblUser tblUser);

	List<TblRole> fetchAllRole();
}
