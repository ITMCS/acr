package com.pims.management.user;

import java.math.BigInteger;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.pims.bean.SessionBean;
import com.pims.common.utility.service.CommonUtility;
import com.pims.data.TblUser;

@Controller
@RequestMapping("/user")
public class UserController {
	
	@Autowired
	UserService userService;
	
	@RequestMapping(value = "/view", method = RequestMethod.GET)
	public String addPostoffice(HttpServletRequest request, Model model) {
		try {
			model.addAttribute("modelUser", new TblUser());
			model.addAttribute("allRole",userService.fetchAllRole());
			model.addAttribute("lstUsers", CommonUtility.convertToJsonString(userService.fetchAllUser()));
						
			return "com.pims.user";
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@ResponseBody
	@RequestMapping(value = "/save", method = RequestMethod.POST)
	public String saveUser(@ModelAttribute("userModel") TblUser user, HttpServletRequest request, Model model) {
		try {
			String resposne = userService.addUser(user);
			return resposne;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public String authenticateUser(@ModelAttribute("userModel") TblUser user, HttpServletRequest request, Model model) {
		try {
			model.addAttribute("userModel", user);			
			TblUser authenticatedUser = userService.authenticateUser(user);
			if (null != authenticatedUser) {
				model.addAttribute("userModel", authenticatedUser);
				SessionBean sessionBean = new SessionBean();
				sessionBean.setUser(authenticatedUser);
				CommonUtility.setToSession(request, "AUTHSESSION", sessionBean);				
				return "redirect:/index";
			} else {
				model.addAttribute("resposneMessage", "Invalid username or password!");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return "beforeAuth/login";
	}
	
	
	@ResponseBody
	@RequestMapping(value = "/edit/{userId}", method = RequestMethod.GET)
	public String edit(@PathVariable("userId")BigInteger userId,HttpServletRequest request, Model model) {
		try {
			TblUser user =userService.fetchUserbyId(userId);
			return CommonUtility.convertToJsonString(user);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/fetchAll", method = RequestMethod.GET)
	public String fetchAllUser(HttpServletRequest request, Model model) {
		try {
			String lstUsers = CommonUtility.convertToJsonString(userService.fetchAllUser());
			return lstUsers;
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
	
	@ResponseBody
	@RequestMapping(value = "/delete/{userId}", method = RequestMethod.GET)
	public String delete(@PathVariable("userId")BigInteger userId,HttpServletRequest request, Model model) {
		try {
			String resposneString =userService.deleteUserbyId(userId);
			return resposneString;			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;		
	}
}
