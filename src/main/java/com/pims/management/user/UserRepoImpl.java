package com.pims.management.user;



import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.pims.common.dal.service.*;
import com.pims.common.utility.service.CommonUtility;
import com.pims.data.TblRole;
import com.pims.data.TblUser;

@Repository
public class UserRepoImpl implements UserRepo{

	@Autowired
	CommonDAO commonDao;
	
	
	@Override
	public TblUser addUser(TblUser user) {
		user.setIsActive(1);
		user.setCreatedOn(new Date());
		user.setUpdatedOn(new Date());
		commonDao.saveOrUpdate(user);
		return user;
	}


	@Override
	public TblUser fetchUserbyUserName(String userName) {		
		List<TblUser> lstUser  = commonDao.findEntity(TblUser.class, "userName",OperationTypeEnum.EQ ,userName);
		TblUser dbObjTblUser = null;
		if(!lstUser.isEmpty()) {
			dbObjTblUser =  lstUser.get(0);
		}
		return dbObjTblUser;
	}
	
	@Override
	public TblUser fetchUserbyId(BigInteger userID) {		
		List<TblUser> lstUser  = commonDao.findEntity(TblUser.class, "userPk",OperationTypeEnum.EQ ,userID);
		TblUser dbObjTblUser = new TblUser();
		if(!lstUser.isEmpty()) {
			dbObjTblUser =  lstUser.get(0);
		}
		return dbObjTblUser;
	}


	@Override
	public List<TblUser> fetchAllUser() {
		List<TblUser> lstUser  = commonDao.findEntity(TblUser.class);
		return lstUser;
	}


	@Override
	public boolean deleteUser(TblUser tblUser) {
		commonDao.delete(tblUser);
		return true;
	}


	@Override
	public String updateUser(TblUser user) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<TblRole> fetchAllRole() {
		List<TblRole> lstRole  = commonDao.findEntity(TblRole.class);
		return lstRole;
	}
	
}
