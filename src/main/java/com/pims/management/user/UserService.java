package com.pims.management.user;

import java.math.BigInteger;
import java.util.List;

import org.springframework.stereotype.Service;

import com.pims.data.TblRole;
import com.pims.data.TblUser;

@Service
public interface UserService {

	String addUser(TblUser user);
	
	TblUser authenticateUser(TblUser user);
	
	TblUser fetchUserByUsername(String username);

	List<TblUser> fetchAllUser();

	String deleteUserbyId(BigInteger userId);

	TblUser fetchUserbyId(BigInteger userId);
	
	List<TblRole> fetchAllRole();
}
