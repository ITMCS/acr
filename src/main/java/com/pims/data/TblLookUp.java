package com.pims.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tbl_LOOKUP")
public class TblLookUp {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "LOOKUPPK", sequenceName = "LOOKUP_SEQ")
	private BigInteger lookUpPk;
	
	@Column(name = "LOOKUPNAME")
	 private String lookUpName;
	
	@Column(name = "LOOKUPNATIVENAME")
	 private String lookUpNativeName;
	
	@Column(name = "KEYWORD")
	 private String keyword;
	
	@Column(name = "ABBREVIATION")
	 private String abbreviation;
	
	@Column(name = "ABBREVIATIONNATIVE")
	 private String abbreviationNative;
	
	@Column(name = "SINO")
	 private String siNo;
	
	@Column(name = "ISACTIVE")
	 private int isActive;
	 
	 @ManyToOne
	 @JoinColumn(name = "CREATEDBY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATEDON")
	 private Date createdOn;
	 
	 @ManyToOne
	 @JoinColumn(name = "UPDATEDBY")
	 private TblUser updatedBy;
	 
	 @Column(name = "UPDATEDON")
	 private Date updatedOn;

	 public BigInteger getLookUpPk() {
			return lookUpPk;
		}

		public void setLookUpPk(BigInteger lookUpPk) {
			this.lookUpPk = lookUpPk;
		}

		public String getLookUpName() {
			return lookUpName;
		}

		public void setLookUpName(String lookUpName) {
			this.lookUpName = lookUpName;
		}

		public String getLookUpNativeName() {
			return lookUpNativeName;
		}

		public void setLookUpNativeName(String lookUpNativeName) {
			this.lookUpNativeName = lookUpNativeName;
		}

		public String getKeyword() {
			return keyword;
		}

		public void setKeyword(String keyword) {
			this.keyword = keyword;
		}

		public String getAbbreviation() {
			return abbreviation;
		}

		public void setAbbreviation(String abbreviation) {
			this.abbreviation = abbreviation;
		}

		public String getAbbreviationNative() {
			return abbreviationNative;
		}

		public void setAbbreviationNative(String abbreviationNative) {
			this.abbreviationNative = abbreviationNative;
		}

		public String getSiNo() {
			return siNo;
		}

		public void setSiNo(String siNo) {
			this.siNo = siNo;
		}
	 
		public int getIsActive() {
			return isActive;
		}
	
		public void setIsActive(int isActive) {
			this.isActive = isActive;
		}
	
		public TblUser getCreatedBy() {
			return createdBy;
		}
	
		public void setCreatedBy(TblUser createdBy) {
			this.createdBy = createdBy;
		}
	
		public Date getCreatedOn() {
			return createdOn;
		}
	
		public void setCreatedOn(Date createdOn) {
			this.createdOn = createdOn;
		}
	
		public TblUser getUpdatedBy() {
			return updatedBy;
		}
	
		public void setUpdatedBy(TblUser updatedBy) {
			this.updatedBy = updatedBy;
		}
	
		public Date getUpdatedOn() {
			return updatedOn;
		}
	
		public void setUpdatedOn(Date updatedOn) {
			this.updatedOn = updatedOn;
		}
	}