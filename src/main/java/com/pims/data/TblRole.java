package com.pims.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TBL_ROLE")
public class TblRole {

	@Id
	 @GeneratedValue(strategy = GenerationType.SEQUENCE)
	 @SequenceGenerator(name = "ROLEPK", sequenceName = "ROLE_SEQ")
	 private BigInteger rolePk;
	
	 @Column(name = "ROLENAME")
	 private String roleName;
	 
	 
	 @Column(name = "ISACTIVE")
	 private int isActive;
	 
	 
	 @Column(name = "CREATEDON")
	 private Date createdOn;
	 


	public Date getCreatedOn() {
		return createdOn;
	}


	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}



	public Date getUpdatedOn() {
		return updatedOn;
	}


	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}

 
	 @Column(name = "UPDATEDON")
	 private Date updatedOn;


	public BigInteger getRolePk() {
		return rolePk;
	}


	public void setRolePk(BigInteger rolePk) {
		this.rolePk = rolePk;
	}


	public String getRoleName() {
		return roleName;
	}


	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}


	public int getIsActive() {
		return isActive;
	}


	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	 
	 
	 
}
