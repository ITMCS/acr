/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pims.data;

import java.math.BigInteger;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ITMCS-1
 */
@Entity
@Table(name="TBL_ACRDOCMAP")
public class TblACRDocumentMap {
    
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "ACRDOCPK", sequenceName = "ACRDOC_SEQ")
    private BigInteger acrDocPk;
    
    @ManyToOne
    @JoinColumn(name = "ACRPK")
    private TblACR acrPk;
    
    @Column(name = "DOCID")
    private BigInteger docId;
    
    @Column(name = "ISACTIVE")
    private int isActive;

    public BigInteger getAcrDocPk() {
        return acrDocPk;
    }

    public void setAcrDocPk(BigInteger acrDocPk) {
        this.acrDocPk = acrDocPk;
    }

    public TblACR getAcrPk() {
        return acrPk;
    }

    public void setAcrPk(TblACR acrPk) {
        this.acrPk = acrPk;
    }

    public BigInteger getDocId() {
        return docId;
    }

    public void setDocId(BigInteger docId) {
        this.docId = docId;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }
    
}
