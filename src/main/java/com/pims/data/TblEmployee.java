package com.pims.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name="Tbl_Employee")
public class TblEmployee {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "EMPLOYEEPK", sequenceName = "EMPLOYEE_SEQ")
	private BigInteger employeePk;
	
	@Column(name = "GOVID")
	private String govId;
	
	@Column(name = "ENGNAME")
	private String engName;
	
	@Column(name = "BNGNAME")
	private String bngName;
	
	@Column(name = "BNGFATHERNAME")
	private String bngFatherName;
	
	@Column(name = "ENGFATHERNAME")
	private String engFatherName;
	
	@Column(name = "BNGMOTHERNAME")
	private String bngMotherName;
	
	@Column(name = "ENGMOTHERNAME")
	private String engMotherName;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "BIRTHDATE")
	private Date birthDate;
	
	@Column(name = "GENDER")
	private String gender;
	
	@Column(name = "IMAGEPATH")
	private String imagePath;
	
	@Column(name = "PREFIXNAME")
	private String preFixName;
	
	@Column(name = "SUFFIXNAME")
	private String suffixName;
	
	@Column(name = "MATIRIAL_STATUS")
	private String matirialStatus;
	
	@Column(name = "HOME_DISTRICT")
	private String homeDistrict;
	
	@Column(name = "RELIGION")
	private String religion;
	
	@Column(name = "JOIN_TYPE")
	private String joinType;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "JOIN_DATE")
	private String joinDate;
	
	@Column(name = "FREEDOM_FIGHTER")
	private String freedomFighter;
	
	@Column(name = "BATCH")
	private String batch;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "CONFIRMATION_GO_DATE")
	private Date confirmationGoDate;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "CONFIRMATION_DATE")
	private Date confirmationDate;	
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@Column(name = "EXPIRY_DATE")
	private Date expiryDate;
	
	@Column(name = "PROMOTE")
	private String promote;
	
	@Column(name = "ACTIVITY_STATUS")
	private String activeStatus;
	
	@Column(name = "PLACEMENT")
	private String placement;
	
	@Column(name = "ADDITIOANL_PERSONAL_INFO")
	private String additionalPersonalInfo;
	
	@Column(name = "REMARKS")
	private String remarks;

	public BigInteger getEmployeePk() {
		return employeePk;
	}

	public void setEmployeePk(BigInteger employeePk) {
		this.employeePk = employeePk;
	}

	public String getGovId() {
		return govId;
	}

	public void setGovId(String govId) {
		this.govId = govId;
	}

	public String getEngName() {
		return engName;
	}

	public void setEngName(String engName) {
		this.engName = engName;
	}

	public String getBngName() {
		return bngName;
	}

	public void setBngName(String bngName) {
		this.bngName = bngName;
	}

	public String getBngFatherName() {
		return bngFatherName;
	}

	public void setBngFatherName(String bngFatherName) {
		this.bngFatherName = bngFatherName;
	}

	public String getEngFatherName() {
		return engFatherName;
	}

	public void setEngFatherName(String engFatherName) {
		this.engFatherName = engFatherName;
	}

	public String getBngMotherName() {
		return bngMotherName;
	}

	public void setBngMotherName(String bngMotherName) {
		this.bngMotherName = bngMotherName;
	}

	public String getEngMotherName() {
		return engMotherName;
	}

	public void setEngMotherName(String engMotherName) {
		this.engMotherName = engMotherName;
	}

	public Date getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(Date birthDate) {
		this.birthDate = birthDate;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getImagePath() {
		return imagePath;
	}

	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}

	public String getPreFixName() {
		return preFixName;
	}

	public void setPreFixName(String preFixName) {
		this.preFixName = preFixName;
	}

	public String getSuffixName() {
		return suffixName;
	}

	public void setSuffixName(String suffixName) {
		this.suffixName = suffixName;
	}

	public String getMatirialStatus() {
		return matirialStatus;
	}

	public void setMatirialStatus(String matirialStatus) {
		this.matirialStatus = matirialStatus;
	}

	public String getHomeDistrict() {
		return homeDistrict;
	}

	public void setHomeDistrict(String homeDistrict) {
		this.homeDistrict = homeDistrict;
	}

	public String getReligion() {
		return religion;
	}

	public void setReligion(String religion) {
		this.religion = religion;
	}

	public String getJoinType() {
		return joinType;
	}

	public void setJoinType(String joinType) {
		this.joinType = joinType;
	}

	public String getJoinDate() {
		return joinDate;
	}

	public void setJoinDate(String joinDate) {
		this.joinDate = joinDate;
	}

	public String getFreedomFighter() {
		return freedomFighter;
	}

	public void setFreedomFighter(String freedomFighter) {
		this.freedomFighter = freedomFighter;
	}

	public String getBatch() {
		return batch;
	}

	public void setBatch(String batch) {
		this.batch = batch;
	}

	public Date getConfirmationGoDate() {
		return confirmationGoDate;
	}

	public void setConfirmationGoDate(Date confirmationGoDate) {
		this.confirmationGoDate = confirmationGoDate;
	}

	public Date getConfirmationDate() {
		return confirmationDate;
	}

	public void setConfirmationDate(Date confirmationDate) {
		this.confirmationDate = confirmationDate;
	}

	public Date getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(Date expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getPromote() {
		return promote;
	}

	public void setPromote(String promote) {
		this.promote = promote;
	}

	public String getActiveStatus() {
		return activeStatus;
	}

	public void setActiveStatus(String activeStatus) {
		this.activeStatus = activeStatus;
	}

	public String getPlacement() {
		return placement;
	}

	public void setPlacement(String placement) {
		this.placement = placement;
	}

	public String getAdditionalPersonalInfo() {
		return additionalPersonalInfo;
	}

	public void setAdditionalPersonalInfo(String additionalPersonalInfo) {
		this.additionalPersonalInfo = additionalPersonalInfo;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}
	
}
