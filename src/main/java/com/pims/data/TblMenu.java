package com.pims.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="TBL_MENU")
public class TblMenu {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "MENUPK", sequenceName = "MENU_SEQ")
	private BigInteger menuPk;
	
	@Column(name = "NAME")
	 private String name;
	
	@Column(name = "URL")
	 private String url;
	
	@Column(name = "ICON")
	private String icon;
	
	
	 @Column(name = "PARENT")
	 private int parent;
	 
	 @Column(name = "CHILD")
	 private int child;
	 
	 @Column(name = "ISACTIVE")
	 private int isActive;
	 
	 @Column(name = "SELF_PARENT")
	 private BigInteger parentSelfId;
	 
	 @ManyToOne
	 @JoinColumn(name = "CREATEDBY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATEDON")
	 private Date createdOn;
	 
	 @ManyToOne
	 @JoinColumn(name = "UPDATEDBY")
	 private TblUser updatedBy;
	 
	 @Column(name = "UPDATEDON")
	 private Date updatedOn;

	public BigInteger getMenuPk() {
		return menuPk;
	}

	public BigInteger getParentSelfId() {
		return parentSelfId;
	}

	public void setParentSelfId(BigInteger parentSelfId) {
		this.parentSelfId = parentSelfId;
	}

	public void setMenuPk(BigInteger menuPk) {
		this.menuPk = menuPk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}


	public int getParent() {
		return parent;
	}

	public void setParent(int parent) {
		this.parent = parent;
	}

	public int getChild() {
		return child;
	}

	public void setChild(int child) {
		this.child = child;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	
}
