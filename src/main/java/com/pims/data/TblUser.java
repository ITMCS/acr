package com.pims.data;

import java.math.BigInteger;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name="tbl_USERTABLE")
public class TblUser {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "USERPK", sequenceName = "USER_SEQ")
	private BigInteger userPk;
	
	@Column(name = "NAME")
	 private String name;
	
	@Column(name = "EMAIL")
	 private String email;
	
	 @Column(name = "USERNAME")
	 private String userName;
	 
	 @Column(name = "PASSWORD")
	 private String password;
	 
	@ManyToOne
	@JoinColumn(name = "ROLEFK")
	private TblRole roleFK;
	 
	 @Column(name = "ISACTIVE")
	 private int isActive;
	 
	 @ManyToOne
	 @JoinColumn(name = "CREATEDBY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATEDON")
	 private Date createdOn;
	 
	 @ManyToOne
	 @JoinColumn(name = "UPDATEDBY")
	 private TblUser updatedBy;
	 
	 @Column(name = "UPDATEDON")
	 private Date updatedOn;

	public BigInteger getUserPk() {
		return userPk;
	}

	public void setUserPk(BigInteger userPk) {
		this.userPk = userPk;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}


	public TblRole getRoleFK() {
		return roleFK;
	}

	public void setRoleFK(TblRole roleFK) {
		this.roleFK = roleFK;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
	 
}
