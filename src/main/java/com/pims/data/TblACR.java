/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pims.data;

import java.math.BigInteger;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 *
 * @author ITMCS-1
 */
@Entity
@Table(name="TBL_ACR")
public class TblACR {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @SequenceGenerator(name = "ACRPK", sequenceName = "ACR_SEQ")
    private BigInteger acrPk;

    @ManyToOne
    @JoinColumn(name = "PIMSID")
    private TblEmployee pimsId;
    
    @Column(name = "ACRYEAR")
    private String acrYear;
    
    @Column(name="ISACRREQUIRED")
    private String isAcrRequired;
    
    @Column(name="REASON")
    private String reason;
    
    @Column(name="ACRTYPE")
    private String acrType;
    
    @Column(name="ACRFROM")
    private String from;
    
    @Column(name="ACRTO")
    private String to;
    
    @Column(name="HEALTHREPORT")
    private String healthReport;
    
    @Column(name="HEALTHREPORTDATE")
    private String healthReportDate;
    
    @Column(name="ORUSUBDATE")
    private String oruSubDate;
    
    @Column(name="ORUNO")
    private String oruNo;
    
    @Column(name="CSONO")
    private String csoNo;
    
    @Column(name="ORUREMARK")
    private String oruRemark;
    
    @Column(name="CSOREMARK")
    private String csoRemark;
    
    @Column(name="IFORUNEGREMARK")
    private String ifOruNegRemark;
    
    @Column(name="IFCSONEGREMARK")
    private String ifCsoNegRemark;
    
    @Column(name="NEGREMARKSBYORU")
    private String negremarksByoru;
    
    @Column(name="NEGREMARKSBYCSO")
    private String negremarksByCSO;
    
    @Column(name="IFSUGBYORU")
    private String ifSugBYOru;
    
    @Column(name="IFSUGBYCSO")
    private String ifSugByCso;
    
    @Column(name="SUGBYORU")
    private String sugByOru;
    
    @Column(name="SUGBYCSO")
    private String subByCso;
    
    @Column(name="ORUID")
    private String oruId;
    
    @Column(name="CSOID")
    private String csoId;
    
    @Column(name="ORUOFFICELOC")
    private String oruOfficeLoc;
    
    @Column(name="CSOOFFICELOC")
    private String csoOfficeLoc;
    
    @Column(name="ORUSIGNDATE")
    private String oruSignDate;
    
    @Column(name="CSOSIGNDATE")
    private String csoSignDate;
    
    @Column(name="ORUSUBMISSIONDATE")
    private String oruSubMissionDate;
    
    @Column(name="CSOSUBMISSIONDATE")
    private String csoSubMissionDate;
    
    @Column(name="RECEIVERID")
    private String receiverId;
    
    @Column(name="RECEIVERDATE")
    private String receiverDate;
    
    @Column(name="DECISIONFORNEGREMARK")
    private String decisionForNegRemark;
    
    @Column(name="MEMNO")
    private String memNo;
    
    @Column(name="MEMDATE")
    private String memDate;
    
    @Column(name="OPACRYEAR")
    private String year;
    
    @Column(name="ACRUNTIL")
    private String until;
    
    @Column(name="ACRDESCRIPTION")
    private String description;
    
    @Column(name="REMARK")
    private String remark;
    
    @Column(name = "ISACTIVE")
    private int isActive;

    @ManyToOne
    @JoinColumn(name = "CREATEDBY")
    private TblUser createdBy;

    @Column(name = "CREATEDON")
    private Date createdOn;

    @ManyToOne
    @JoinColumn(name = "UPDATEDBY")
    private TblUser updatedBy;

    @Column(name = "UPDATEDON")
    private Date updatedOn;

    public String getOruId() {
        return oruId;
    }

    public void setOruId(String oruId) {
        this.oruId = oruId;
    }

    public String getCsoId() {
        return csoId;
    }

    public void setCsoId(String csoId) {
        this.csoId = csoId;
    }
    
    
    

    public BigInteger getAcrPk() {
        return acrPk;
    }

    public void setAcrPk(BigInteger acrPk) {
        this.acrPk = acrPk;
    }

    public TblEmployee getPimsId() {
        return pimsId;
    }

    public void setPimsId(TblEmployee pimsId) {
        this.pimsId = pimsId;
    }

    public String getAcrYear() {
        return acrYear;
    }

    public void setAcrYear(String acrYear) {
        this.acrYear = acrYear;
    }

    public String getIsAcrRequired() {
        return isAcrRequired;
    }

    public void setIsAcrRequired(String isAcrRequired) {
        this.isAcrRequired = isAcrRequired;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getAcrType() {
        return acrType;
    }

    public void setAcrType(String acrType) {
        this.acrType = acrType;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getHealthReport() {
        return healthReport;
    }

    public void setHealthReport(String healthReport) {
        this.healthReport = healthReport;
    }

    public String getHealthReportDate() {
        return healthReportDate;
    }

    public void setHealthReportDate(String healthReportDate) {
        this.healthReportDate = healthReportDate;
    }

    public String getOruSubDate() {
        return oruSubDate;
    }

    public void setOruSubDate(String oruSubDate) {
        this.oruSubDate = oruSubDate;
    }

    public String getOruNo() {
        return oruNo;
    }

    public void setOruNo(String oruNo) {
        this.oruNo = oruNo;
    }

    public String getCsoNo() {
        return csoNo;
    }

    public void setCsoNo(String csoNo) {
        this.csoNo = csoNo;
    }

    public String getOruRemark() {
        return oruRemark;
    }

    public void setOruRemark(String oruRemark) {
        this.oruRemark = oruRemark;
    }

    public String getCsoRemark() {
        return csoRemark;
    }

    public void setCsoRemark(String csoRemark) {
        this.csoRemark = csoRemark;
    }

    public String getIfOruNegRemark() {
        return ifOruNegRemark;
    }

    public void setIfOruNegRemark(String ifOruNegRemark) {
        this.ifOruNegRemark = ifOruNegRemark;
    }

    public String getIfCsoNegRemark() {
        return ifCsoNegRemark;
    }

    public void setIfCsoNegRemark(String ifCsoNegRemark) {
        this.ifCsoNegRemark = ifCsoNegRemark;
    }

    public String getNegremarksByoru() {
        return negremarksByoru;
    }

    public void setNegremarksByoru(String negremarksByoru) {
        this.negremarksByoru = negremarksByoru;
    }

    public String getNegremarksByCSO() {
        return negremarksByCSO;
    }

    public void setNegremarksByCSO(String negremarksByCSO) {
        this.negremarksByCSO = negremarksByCSO;
    }

    public String getIfSugBYOru() {
        return ifSugBYOru;
    }

    public void setIfSugBYOru(String ifSugBYOru) {
        this.ifSugBYOru = ifSugBYOru;
    }

    public String getIfSugByCso() {
        return ifSugByCso;
    }

    public void setIfSugByCso(String ifSugByCso) {
        this.ifSugByCso = ifSugByCso;
    }

    public String getSugByOru() {
        return sugByOru;
    }

    public void setSugByOru(String sugByOru) {
        this.sugByOru = sugByOru;
    }

    public String getSubByCso() {
        return subByCso;
    }

    public void setSubByCso(String subByCso) {
        this.subByCso = subByCso;
    }

    public String getOruOfficeLoc() {
        return oruOfficeLoc;
    }

    public void setOruOfficeLoc(String oruOfficeLoc) {
        this.oruOfficeLoc = oruOfficeLoc;
    }

    public String getCsoOfficeLoc() {
        return csoOfficeLoc;
    }

    public void setCsoOfficeLoc(String csoOfficeLoc) {
        this.csoOfficeLoc = csoOfficeLoc;
    }

    public String getOruSignDate() {
        return oruSignDate;
    }

    public void setOruSignDate(String oruSignDate) {
        this.oruSignDate = oruSignDate;
    }

    public String getCsoSignDate() {
        return csoSignDate;
    }

    public void setCsoSignDate(String csoSignDate) {
        this.csoSignDate = csoSignDate;
    }

    public String getOruSubMissionDate() {
        return oruSubMissionDate;
    }

    public void setOruSubMissionDate(String oruSubMissionDate) {
        this.oruSubMissionDate = oruSubMissionDate;
    }

    public String getCsoSubMissionDate() {
        return csoSubMissionDate;
    }

    public void setCsoSubMissionDate(String csoSubMissionDate) {
        this.csoSubMissionDate = csoSubMissionDate;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverDate() {
        return receiverDate;
    }

    public void setReceiverDate(String receiverDate) {
        this.receiverDate = receiverDate;
    }

    public String getDecisionForNegRemark() {
        return decisionForNegRemark;
    }

    public void setDecisionForNegRemark(String decisionForNegRemark) {
        this.decisionForNegRemark = decisionForNegRemark;
    }

    public String getMemNo() {
        return memNo;
    }

    public void setMemNo(String memNo) {
        this.memNo = memNo;
    }

    public String getMemDate() {
        return memDate;
    }

    public void setMemDate(String memDate) {
        this.memDate = memDate;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    public String getUntil() {
        return until;
    }

    public void setUntil(String until) {
        this.until = until;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public int getIsActive() {
        return isActive;
    }

    public void setIsActive(int isActive) {
        this.isActive = isActive;
    }

    public TblUser getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(TblUser createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(Date createdOn) {
        this.createdOn = createdOn;
    }

    public TblUser getUpdatedBy() {
        return updatedBy;
    }

    public void setUpdatedBy(TblUser updatedBy) {
        this.updatedBy = updatedBy;
    }

    public Date getUpdatedOn() {
        return updatedOn;
    }

    public void setUpdatedOn(Date updatedOn) {
        this.updatedOn = updatedOn;
    }
    
    
}
