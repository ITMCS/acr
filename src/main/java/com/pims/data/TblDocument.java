package com.pims.data;

import java.math.BigInteger;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.web.multipart.MultipartFile;



/**
 * @author ITMCS Java
 *
 */
@Entity
@Table(name="tbl_DOCUMENT")
public class TblDocument {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	@SequenceGenerator(name = "DOCUMENTPK", sequenceName = "DOCUMENT_SEQ")
	private BigInteger documentPk;
	
	@Column(name = "DOCUMENTNO")
	 private Integer documentNo;
	
	
	@Column(name = "DOCUMENTDATE")
	private String documentDate;
	
	@Column(name = "DOCUMENTTYPE")
	 private String documentType;
	
	@Column(name = "DOCUMENTSUB")
	 private String documentSub;
	
	@Column(name = "ATTACHMENT")
	 private String attachment;
	
	@Column(name = "GID")
	 private String gId;
	
	@Column(name = "REMARKS")
	 private String remarks;
	 
	 @ManyToOne
	 @JoinColumn(name = "CREATEDBY")
	 private TblUser createdBy;
	 
	 @Column(name = "CREATEDON")
	 private Date createdOn;
	 
	 @ManyToOne
	 @JoinColumn(name = "UPDATEDBY")
	 private TblUser updatedBy;
	 
	 @Column(name = "UPDATEDON")
	 private Date updatedOn;
	 
	 @Transient
	 List<MultipartFile> fileDoc;
	 
	public List<MultipartFile> getFileDoc() {
		return fileDoc;
	}

	public void setFileDoc(List<MultipartFile> fileDoc) {
		this.fileDoc = fileDoc;
	}
	 
	public BigInteger getDocumentPk() {
		return documentPk;
	}

	public void setDocumentPk(BigInteger documentPk) {
		this.documentPk = documentPk;
	}

	public Integer getDocumentNo() {
		return documentNo;
	}

	public void setDocumentNo(Integer documentNo) {
		this.documentNo = documentNo;
	}
	

	public String getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(String documentDate) {
		this.documentDate = documentDate;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getDocumentSub() {
		return documentSub;
	}

	public void setDocumentSub(String documentSub) {
		this.documentSub = documentSub;
	}

	public String getAttachment() {
		return attachment;
	}

	public void setAttachment(String attachment) {
		this.attachment = attachment;
	}

	public String getgId() {
		return gId;
	}

	public void setgId(String gId) {
		this.gId = gId;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public TblUser getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(TblUser createdBy) {
		this.createdBy = createdBy;
	}

	public Date getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}

	public TblUser getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(TblUser updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Date updatedOn) {
		this.updatedOn = updatedOn;
	}
}