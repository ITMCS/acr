<%@page contentType="text/html" pageEncoding="UTF-8"%>

<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
<meta name="description"
	content="CoreUI - Open Source Bootstrap Admin Template">
<meta name="author" content="Łukasz Holeczek">
<meta name="keyword"
	content="Bootstrap,Admin,Template,Open,Source,jQuery,CSS,HTML,RWD,Dashboard">

<link rel="icon" type="image/png" href="${pageContext.servletContext.contextPath}/resources/img/brand/favicon.png">
<span class="trn"><title>PIMS</title></span>


<!-- Icons-->
<link
	href="${pageContext.servletContext.contextPath}/resources/css/coreui-icons.min.css"
	rel="stylesheet">
<link
	href="${pageContext.servletContext.contextPath}/resources/css/flag-icon.min.css"
	rel="stylesheet">

<link
	href="${pageContext.servletContext.contextPath}/resources/css/font-awesome.min.css"
	rel="stylesheet">
<link
	href="${pageContext.servletContext.contextPath}/resources/css/simple-line-icons.css"
	rel="stylesheet">
<!-- Main styles for this application-->
<link
	href="${pageContext.servletContext.contextPath}/resources/css/style.css"
	rel="stylesheet">
<link
	href="${pageContext.servletContext.contextPath}/resources/vendors/pace-progress/css/pace.min.css"
	rel="stylesheet">
<link rel="stylesheet" type="text/css" href="${pageContext.servletContext.contextPath}/resources/css/bootstrap.min.css"></style>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.css" />  	
<link rel="stylesheet" href="${pageContext.servletContext.contextPath}/resources/customCss/custom.css"/>

<script	src="${pageContext.servletContext.contextPath}/resources/boot/jquery.min.js"></script>
<script	src="${pageContext.servletContext.contextPath}/resources/translate/jquery.translate.js"></script>

	 
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<!--<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/changeReflect.js"></script>-->

<style>
body, html {
	height: 100%;
	width: 100%;
	min-height: 100%;
	padding: 0;
	margin: 0;
}

mrg-10 {
	margin: -10px !important;
}

.error {
	width: auto;
	height: 20px;
	height: auto;
	position: absolute;
	left: 50%;
	margin-left: -100px;
	bottom: 10px;
	background-color: #e63e3cf0;
	color: #F0F0F0;
	font-family: Calibri;
	font-size: 20px;
	padding: 10px;
	text-align: center;
	border-radius: 7px;
	-webkit-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
	-moz-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
	box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
}

.success {
	width: auto;
	height: 20px;
	height: auto;
	position: absolute;
	border-radius: 7px;
	left: 50%;
	margin-left: -100px;
	bottom: 10px;
	background-color: #0c7230e0;
	color: #F0F0F0;
	font-family: Calibri;
	font-size: 20px;
	padding: 10px;
	text-align: center;
	-webkit-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
	-moz-box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
	box-shadow: 0px 0px 24px -1px rgba(56, 56, 56, 1);
}
.ui-dialog ui-corner-all ui-widget ui-widget-content ui-front ui-draggable ui-resizable{
    top: 66px !important;
    left: 282px !important;
}
.validationMsg {
	color: red;
}

.div-border {
	font-size: 15px;
	border-bottom: 1px solid gray;
}

.btn-danger-no {
	color: #fff;
	background-color: #f86c6b;
	border-color: #f86c6b
}

#loading {
	width: 100%;
	height: 100%;
	top: 0;
	left: 0;
	position: fixed;
	display: block;
	opacity: 0.7;
	background-color: #fff;
	z-index: 99;
	text-align: center;
}

#loading-image {
	position: absolute;
	top: 200px;
	z-index: 100;
}

.ui-widget-overlay {
	background-color: rgba(0, 0, 0, .4);
	opacity: 1;
}

.ui-dialog-mask {
	position: fixed;
	width: 100%;
	height: 100%;
}
</style>
<script>
	window.dataLayer = window.dataLayer || [];
	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());
	// Shared ID
	gtag('config', 'UA-118965717-3');
	// Bootstrap ID
	gtag('config', 'UA-118965717-5');
</script>
	  <script type="text/javascript">
		$(function () {
			$("#dialog").dialog({
				modal: true,
				autoOpen: false,
				title: "Documents",
                                left: 282,
                                top:61,
				width: 900,
				height: 500,                               
				show: {
					effect: "fade",
					duration: 300
				  },
				hide: {
					effect: "fade"
					
				},
				open: function(event, ui) {
					
					$('body').addClass('ui-widget-overlay ui-dialog-mask');
					return true;
					
				},
				close: function(event, ui) {
					
					$('body').removeClass('ui-widget-overlay ui-dialog-mask');
					return true;
				}
			});
		});
		function closePopup(){
			$('#confirmPopMessage').val('');
			$('#dialog').dialog('close');
		}	
		var path='${pageContext.servletContext.contextPath}';		
		function addClass() {
			var element = document.getElementById("myDiv");
			if(element.classList.contains("show"))
				element.classList.remove("show");
			else
				element.classList.add("show");
		}		
			
	</script>


</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">

	<tiles:insertAttribute name="header" />
	<div id="dialog" style="display: none" class="modal" align = "center">
		<div class="div-border">
			<pre></pre>
                        <table aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer " id="mydocTable" role="grid" style="border-collapse: collapse !important">
                            <thead id="theaddoc"></thead>
                            <tbody id="tbodydoc"></tbody>
                        </table>
                        <pre></pre>
                        <input type="hidden" id="confirmPopMessage" value="">
		</div>
		<div>
		<pre></pre>
		<button  class="btn btn-danger-no" onclick="closePopup()"><span class="trn">Close</span></button>  
                </div>
	</div>
	<div class="app-body" id="app-body">
	<tiles:insertAttribute name="menu" />
	<tiles:insertAttribute name="body" />
	</div>
	<div id="footerDiv">
		<tiles:insertAttribute name="footer" />
	</div>
	<div class='error' style='display:none' ><lable id="errorMsg"></lable></div>
	
	<div class='success' style='display:none' ><lable id="successMsg"></lable></div>
	
	
	
	
	<script
		src="${pageContext.servletContext.contextPath}/resources/boot/popper.min.js"></script>
	
	<script
		src="${pageContext.servletContext.contextPath}/resources/boot/pace.min.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/boot/perfect-scrollbar.min.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/boot/coreui.min.js"></script>
	<!-- Plugins and scripts required by this view-->
	<!-- <script src="${pageContext.servletContext.contextPath}/resources/boot/Chart.min.js"></script> -->
	<script
		src="${pageContext.servletContext.contextPath}/resources/boot/custom-tooltips.min.js"></script>
	<script
		src="${pageContext.servletContext.contextPath}/resources/js/main.js"></script>
	
	<script
		src="${pageContext.servletContext.contextPath}/resources/valid/PIMSCommonValidations.js"></script>
		
	<script src="${pageContext.servletContext.contextPath}/resources/valid/PIMSValidationMessage.js"></script>	
	 <script src="https://cdnjs.cloudflare.com/ajax/libs/jqueryui/1.12.1/jquery-ui.js"></script>
	
</body>
</html>