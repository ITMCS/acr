
<div class="sidebar" id="menuchangeforLanguage">
	<nav class="sidebar-nav">
            <ul class="nav">
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/index">
                        <i class="nav-icon icon-speedometer"></i><span class="trn">Dashboard</span>
                    </a>
                </li>
                <li class="nav-item nav-dropdown">
                    <a class="nav-link nav-dropdown-toggle" href=""><i class="nav-icon icon-puzzle"></i><span class="trn">Management</span></a>
                    <ul class="nav-dropdown-items">	
                        <li class="nav-item">
                            <a class="nav-link" href="${pageContext.servletContext.contextPath}/user/view"> 
                                <i class="nav-icon icon-puzzle"></i>
                                <span class="trn">Users Settings</span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="${pageContext.servletContext.contextPath}/acr/view">
                        <i class="nav-icon icon-speedometer"></i><span class="trn">ACR</span>
                    </a>
                </li>
            </ul>
	</nav>
	<button class="sidebar-minimizer brand-minimizer" type="button"></button>
	</div>
