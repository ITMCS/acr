function fnSaveOrganisation() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/organisation/save',
			data : $('form[name=organisationform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br>  record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Organisation record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/organisation/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Organisation records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Organization Type','Parent Organization','Name','Name Native','Location','Organisation Address','Organisation mobile','Organisation Telephone','Organisation Email','Remarks','Action' ];
	createHeader('theadOrganisation',headArr);

		$('#tbodyOrganisation').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyOrganisation'));
				$('<td>'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['organisationType'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['organisationAddress'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['parentOrganisation'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['organisationMobile'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['organisationName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['organisationTelephone'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['nameNative'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['organisationEmail'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['location'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="organisation" value="'+object['organisationPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="organisation" value="'+object['organisationPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}
	//createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#organisationType').val(json['organisationType']);
	$('#organisationAddress').val(json['organisationAddress']);
	$('#parentOrganisation').val(json['parentOrganisation']);
	$('#organisationMobile').val(json['organisationMobile']);
	$('#organisationName').val(json['organisationName']);
	$('#organisationTelephone').val(json['organisationTelephone']);	
	$('#nameNative').val(json['nameNative']);
	$('#organisationEmail').val(json['organisationEmail']);
	$('#location').val(json['location']);
	$('#remarks').val(json['remarks']);
	$('#organisationPk').val(json['organisationPk'])
}

function resetForm(){
	$('#organisationType').val('-1');
	$('#organisationAddress').val('');
	$('#parentOrganisation').val('-1');
	$('#organisationMobile').val('');
	$('#organisationName').val('');
	$('#organisationTelephone').val('');	
	$('#nameNative').val('');
	$('#organisationEmail').val('');
	$('#location').val('-1');
	$('#remarks').val('');
}