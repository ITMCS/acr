/**
 * 
 */
function fnSaveLocation() {
	if(valOnSubmit()){
	startLoading();
		$.post({
			url : path + '/location/save',
			data : $('form[name=locationform]').serialize(),
			success : function(response) {
				console.log(response);
				if(response == 'successFullySaved'){
					showSuccess('<b>Success!</b><br> Location record is sucessfully created');
					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Location record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
	}

function afterSaveDataGet(){
	window.location.href=path+'/location/view';
//	$.get({
//		url : path + '/location/fetchAll',
//		success : function(response) {			
//			if(response != ''){
//				resetForm();
//				prepareGrid(response);
//			}else{
//				showError('<b>ERROR!</b><br> Location records are not fetch sucessfully');	
//				hideLoading();
//			}				
//		}
//	})
}

function setupForm(response){
	var json = JSON.parse(response);
	$('#locationName').val(json['locationName']);
	$('#name_native').val(json['locationNativeName']);
	$('#type').val(json['locationType']);
	$('#isactive').val(json['isActive']);
	$('#location_id').val(json['locationPk']);
}

function prepareGrid(leadData){
	if(checkValue(leadData) != '-'){
		leadData = JSON.parse(leadData);		
		$('#tbodyLocations').html('');
		var count = 0;
		$.each(leadData, function( index, object ) {
			count++;
			var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyLocations'));
			$('<td>'+count+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue(object['locationName'])+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue(object['locationNativeName'])+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			$('<td>'+checkValue('-')+'</td>').appendTo(trTableBody);
			
			$('<td><button class="btn btn-info" type="button" module="location" value="'+object['locationPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="location" value="'+object['locationPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			

			
					  
		});			
		$('#myTable').DataTable({
		 	 "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
		 });
		hideLoading();
	}
}

function resetForm(){
	$('#locationName').val('');
	$('#name_native').val('');
	$('#type').val('');
	$('#isactive').val('-1');
}