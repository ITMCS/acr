function fnSavePayScale() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/payscale/save',
			data : $('form[name=PayScaleform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> PayScale record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> PayScale record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/payscale/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> PayScale records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
	console.log(leadData);
	destroyTable('myTable');
	var headArr = ['Id','Scale','Grade','Pay Scale Year','Action'];
	createHeader('theadpayscale',headArr);
	
		$('#tbodypayscale').empty();
		
		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodypayscale'));
				$('<td>'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['scale'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['grade'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['year'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="payscale" value="'+object['payScalePk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="payscale" value="'+object['payScalePk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}
	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#scale').val(json['scale']);
	$('#grade').val(json['grade']);
	$('#year').val(json['year']);
	$('#payScalePk').val(json['payScalePk'])
}

function resetForm(){
	$('#scale').val('');
	$('#grade').val('');
	$('#year').val('');
	$('#isactive').val('-1');
}
