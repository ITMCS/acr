function fnSaveMenu() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/menu/save',
			data : $('form[name=menuform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Menu record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Menu record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/menu/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Menu records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			

	destroyTable('myTable');
	var headArr = ['Menu Name','Menu URL','Menu Icon','Parent','Action'];
	createHeader('theadMenu',headArr);

		$('#tbodyMenu').empty();
		if(checkValue(leadData) != '-'){			
			leadData = JSON.parse(leadData);			
			var count = 0;
			$.each(leadData, function( index, object ) {
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyMenu'));				
				$('<td>'+checkValue(object['name'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['url'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['icon'])+'</td>').appendTo(trTableBody);
				$('<td>'+parentName(leadData,object)+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="menu" value="'+object['menuPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="menu" value="'+object['menuPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}
	createTable('myTable');	
	hideLoading();
	$('#footerDiv').load(document.URL +  ' #footerDiv');
	fetchDataFromSession();
}

function parentName(leadData, objectGrid) {
	var value = "-";
	if (checkValue(objectGrid['parentSelfId']) != '-') {
		var parentObj = getObjects(leadData, 'menuPk',
				objectGrid['parentSelfId']);
		parentObj = parentObj[0];
		value = parentObj['name'];
	}
	return value;
}

function getObjects(obj, key, val) {	
    var objects = [];
    for (var i in obj) {
        if (!obj.hasOwnProperty(i)) continue;
        if (typeof obj[i] == 'object') {
            objects = objects.concat(getObjects(obj[i], key, val));
        } else if (i == key && obj[key] == val) {
            objects.push(obj);
        }
    }
    return objects;
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#menuPk').val(json['menuPk']);
	$('#name').val(json['name']);
	$('#url').val(json['url']);
	$('#icon').val(json['icon']);
	$('#isactive').val(json['isActive']);
	$('#parent').val(json['parent']);
	$('#child').val(json['child']);
	$('#parentSelfId').val(json['parentSelfId']);
	fnChkParentType();
}

function resetForm(){
	$('#menuPk').val('');
	$('#name').val('');
	$('#url').val('');
	$('#icon').val('');
	$('#isactive').val('-1');
	$('#parent').val('-1');
	$('#child').val('-1');
	fnChkParentType();
}

function fnChkParentType(){
	if($('#parent').val() == 1 && checkValue(leadData) != '-'){
		$('#parentRecord').show();
		$('#parentSelfId').attr("onblur","validateCombo(this);");
		$('#parentSelfId').attr("isrequired","true");
	}else{
		$('#parentRecord').hide();
		$('#parentSelfId').removeAttr("onblur");
		$('#parentSelfId').removeAttr("isrequired");
	}	
}


