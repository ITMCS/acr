function fnSaveDocument() {	
	
	   
	    var $inputs = $('form[name=documentform] :input');
	    var values = {};
	    $inputs.each(function() {
	        values[this.name] = $(this).val();
	    });
	    
	    
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/document/save',
			enctype: 'multipart/form-data',
			data : $('form[name=documentform]'),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Document record is sucessfully created');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Document record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
//	window.location.href = path + '/document/view';
	$.get({
		url : path + '/document/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Document records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			

	destroyTable('myTable');
	var headArr = ['Id','Document No','Document Date','DocumentType','Document Sub Type','Attachment','Government Id','Remarks','Action'];
	createHeader('theadDocument',headArr);

		$('#tbodyDocument').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyDocument'));
				$('<td>'+count+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['documentNo'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['documentDate'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['documentType'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['documentSub'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['attachment'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['gId'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td style="width: 9%"><button class="btn btn-info" type="button" module="document" value="'+object['documentPk']+'" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="document" value="'+object['documentPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			});
		}
	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#documentNo').val(json['documentNo']);
	$('#documentDate').val(json['documentDate']);
	$('#documentType').val(json['documentType']);
	$('#documentSub').val(json['documentSub']);
	$('#attachment').val(json['attachment']);
	$('#gId').val(json['gId']);
	$('#remarks').val(json['remarks']);
	$('#documentPk').val(json['documentPk'])
}

function resetForm(){
	$('#documentNo').val('');
	$('#documentDate').val('');
	$('#documentType').val('-1');
	$('#documentSub').val('-1');
	$('#attachment').val('');
	$('#gId').val('');
	$('#remarks').val('');
}
