function fnSaveaward() {
	if(valOnSubmit()){
		startLoading();
		$.post({
			url : path + '/award/save',
			data : $('form[name=awardform]').serialize(),
			success : function(response) {
				if(response == 'SuccessFullySaved'){
					showSuccess('<b>Success!</b><br> Award record is sucessfully saved');					
					afterSaveDataGet();
				}else{
					showError('<b>ERROR!</b><br> Award record is not sucessfully created');	
					hideLoading();
				}				
			}
		});
	}
}

function afterSaveDataGet(){
	$.get({
		url : path + '/award/fetchAll',
		success : function(response) {			
			if(response != ''){
				resetForm();
                                hideShow();
				prepareGrid(response);
			}else{
				showError('<b>ERROR!</b><br> Award records are not fetch sucessfully');	
			}				
		}
	})
}

function prepareGrid(leadData){			
console.log(leadData);
	
	destroyTable('myTable');
	var headArr = ['Id','Employee Details','Award Name','Award Serial','Ground','Remarks','Action' ];
	createHeader('theadGeneralInformation',headArr);

		$('#tbodyGeneralInformation').empty();

		if(checkValue(leadData) != '-'){
			leadData = JSON.parse(leadData);
			var count = 0;
			$.each(leadData, function( index, object ) {
				count++;
				var trTableBody = $('<tr class="text-center"></tr>').appendTo($('#tbodyGeneralInformation'));
				$('<td>'+count+'</td>').appendTo(trTableBody);
				$('<td>-</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['awardName'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['awardSerial'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['ground'])+'</td>').appendTo(trTableBody);
				$('<td>'+checkValue(object['remarks'])+'</td>').appendTo(trTableBody);
				$('<td><button class="btn btn-info" type="button" module="award" value="'+object['awardPk']+'" onclick="fnEdit(this);showHide()" ><b><i class="fa fa-edit"></i></b></button> &nbsp;&nbsp;<button class="btn btn-danger" type="button" module="award" value="'+object['awardPk']+'" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>').appendTo(trTableBody);
			
			});
		}

	createTable('myTable');	
	hideLoading();
}

function setupForm(response) {
	var json = JSON.parse(response);
	$('#awardName').val(json['awardName']);
	$('#awardSerial').val(json['awardSerial']);
	$('#ground').val(json['ground']);
	$('#remarks').html(json['remarks']);
	$('#awardPk').val(json['awardPk']);
}

function resetForm(){
	$('#awardName').val('');
	$('#awardSerial').val('');
	$('#ground').val('');
	$('#remarks').html('');
	$('#awardPk').val('');
}