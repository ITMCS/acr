
function showError(message){
	
	$('#errorMsg').html('');
	$('#errorMsg').html(message);
	$('.error').stop().fadeIn(400).delay(3000).fadeOut(400);
}

function showSuccess(message){
	
	$('#successMsg').html('');
	$('#successMsg').html(message);
	$('.success').stop().fadeIn(400).delay(3000).fadeOut(400);
}

function checkValue(val){
	
	if(val== '' || val == undefined || val == 'undefined' || val == null || val == 'null'){
		val ='-';
	}
		return val;	
		
}



function startLoading(){
	$('#app-body').prepend('<div id="loading"><img id="loading-image" src="'+path+'/resources/img/Ripple-1s-128px.gif" alt="Loading..." /></div>');
}

function hideLoading(){
	$('#loading').remove();
}

function fnDelete(e){
	$('#dialog').dialog('open');
	$('#yes').attr('onclick','fnExecuteDelete("'+$(e).attr('module')+'",'+$(e).attr('value')+')');
}

function fnEdit(e){
	startLoading();
	$.get({
		url : path +'/'+ $(e).attr('module') +"/edit/"+$(e).attr('value'),
		success : function(response) {
			setupForm(response);
			hideLoading();
		}
	});
}

function destroyTable(tableId) {
	if ($.fn.DataTable.isDataTable('#' + tableId)) {
		$('#' + tableId).DataTable().destroy();
	}
}

function createTable(tableId) {	
	$('#' + tableId).DataTable({
		"autoWidth":false
	    , "JQueryUI":true
	    ,"searchHighlight": true
	    ,"lengthMenu" : [ [ 5, 10, 25, 50, -1 ], [ 5, 10, 25, 50, "All" ] ]
	});
}

function createHeader(headID, headArr){
	$('#'+headID).empty();
	var trHead= $('<tr class="text-center"></tr>').appendTo($('#'+headID));	
	
	for(var i=0; i< headArr.length ;i++){
		$('<td><b><span class="trn">'+headArr[i]+'</span></b></td>').appendTo(trHead);
	}
}

function fnExecuteDelete(module,id){
	console.log(path +'/'+ module +"/delete/"+id);
	closePopup();
	startLoading();
	$.get({
		url : path +'/'+ module +"/delete/"+id,
		success : function(response) {	
			if(response == 'DELETEDSUCCSSFULLY'){					
				showSuccess('<b>Success!</b><br> Record is deleted successfully.');
			}else{
				showError('<b>ERROR!</b><br> Record can not sucessfully delete.');				
			}
			afterSaveDataGet();							
		}
	});
}


