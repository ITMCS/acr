<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
startLoading();
</script>

<main class="main"> <!-- Breadcrumb-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">Document Attachment Information</span></li>
</ol>


<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">Document Information</span></strong>
						</div>
						<form:form role="form" name="documentform" action="${pageContext.servletContext.contextPath}/document/save" onsubmit="return false;" method="post" modelAttribute="modelDocument" enctype="multipart/form-data">
						<div class="card-body">
							<form:hidden path="documentPk" name="documentPk"  id="documentPk"/>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="documentNo"><span class="trn">Document No</span></label>
										<div class="col-md-9">
											<form:input path="documentNo"
											title="Document No" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="documentNo" id="documentNo"
												name="documentNo" placeholder="Enter Document No"
												type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-4"></div>
							</div>	
							
						<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="documentDate"><span class="trn">Document Date</span></label>
										<div class="col-md-9">
											<form:input path="documentDate" 
											title="Document Date" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="documentDate" id="documentDate"
												name="documentDate" placeholder="Enter Document Date"
												type="date"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="documentType"><span class="trn">Document Type</span></label>
										<div class="col-md-9">
											<form:select path="documentType"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="documentType" id="documentType" name="documentType" onblur="validateCombo(this);" isrequired="true" title="Document Type">
												<form:option value="-1" selected="selected" >Select Document Type</form:option>
												<form:option value="General Information">General Information</form:option>
												<form:option value="Additional Qualification">Additional Qualification</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="documentSub"><span class="trn">Document Sub Type</span></label>
										<div class="col-md-9">
											<form:select path="documentSub"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="documentSub" id="documentSub" name="documentSub" onblur="validateCombo(this);" isrequired="true" title="Document Sub Type">
												<form:option selected="selected" value="-1">Select Document Sub Type</form:option>
												<form:option value="NID">NID</form:option>
												<form:option value="SSC">SSC</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
<!-- 							<div class="row"> -->
<!-- 								<div class="col-sm-8"> -->
<!-- 									<div class="form-group row"> -->
<%-- 										<label class="col-md-3 col-form-label" for="attachment"><span class="trn">Attachment</span></label> --%>
<!-- 										<div class="col-md-9"> -->
<%-- 											<form:input path="file"				 --%>
<%-- 												class="form-control ng-untouched ng-pristine ng-invalid" --%>
<%-- 												formcontrolname="attachment" id="attachment" --%>
<%-- 												name="file" placeholder="Enter Attachment" type="file"/> --%>
												
<!-- 											<div class="ng-star-inserted"> -->
<!-- 											</div> -->
<!-- 										</div> -->
<!-- 									</div> -->

<!-- 								</div> -->
<!-- 								<div class="col-sm-4"></div> -->
<!-- 							</div> -->
							
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="gId"><span class="trn">Government IDs with comma separated</span></label>
										<div class="col-md-9">
											<form:input path="gId"
											title="Document Date" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="gId" id="gId"
												name="gId" placeholder="Enter Government IDs with comma separated"
												type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="remarks"><span class="trn">Remarks</span></label>
										<div class="col-md-9">
											<form:input path="remarks"
											title="Remarks" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="remarks" id="remarks"
												name="remarks" placeholder="Enter Remarks"
												type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-4"></div>
							</div>
							
						</div>
						
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveDocument()">
								<i class="fa fa-dot-circle-o"></i><span class="trn"> Save</span>
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadDocument"></thead>								
										<tbody id="tbodyDocument"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>

<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/document.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstDocument}';
prepareGrid(leadData);
</script>