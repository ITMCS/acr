<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
startLoading();
</script>
<main class="main"> <!-- Breadcrumb-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">Users Settings Component</span></li>
</ol>
<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">Users Settings</span></strong>
						</div>
						<form:form role="form" name="userform" action="${pageContext.servletContext.contextPath}/user/save" onsubmit="return false;" method="post" modelAttribute="modelUser" >
						<div class="card-body">
							<form:hidden path="userPk" name="userPk"  id="userPk"/>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name">Name</label>
										<div class="col-md-9">
											<form:input path="name" class="form-control ng-untouched ng-pristine ng-invalid"
											title="Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="name" id="name" name="name"
												placeholder="Enter Name" />
											<div class="ng-star-inserted">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name_native">Username</label>
										<div class="col-md-9">
											<form:input path="userName"
											title="Username" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="userName" id="userName"
												name="userName" placeholder="Enter Username"
												/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name_native">Email</label>
										<div class="col-md-9">
											<form:input path="email"
											title="Email" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="email" id="email"
												name="email" placeholder="Enter Email"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name_native">Password</label>
										<div class="col-md-9">
											<form:password path="password"
											title="Password" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="password" id="password"
												name="password" placeholder="Enter password"
												/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive">Role</label>
										<div class="col-md-9">
											<form:select path="roleFK.rolePk" style="width: 15%;"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="roleFK" id="roleFK.rolePk" name="roleFK.rolePk" onblur="validateCombo(this);" isrequired="true" title="Role Type">
												<form:option disabled="true" selected="selected"  value="-1">Choose</form:option>
												<c:forEach items="${allRole}" var="entry">
													<form:option value="${entry.rolePk}">${entry.roleName}</form:option>	
												</c:forEach>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
						</div>
						
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveUser()"
								disabled="">
								<i class="fa fa-dot-circle-o"></i> Save
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadUser"></thead>								
										<tbody id="tbodyUser"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/management/userManage.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstUsers}';
prepareGrid(leadData);
</script>