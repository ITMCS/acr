<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script>
startLoading();
</script>
<main class="main"> <!-- Breadcrumb-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">Menu Management</span></li>
</ol>
<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">Menu</span></strong>
						</div>
						<form:form role="form" name="menuform" action="${pageContext.servletContext.contextPath}/menu/save" onsubmit="return false;" method="post" modelAttribute="modelMenu" >
						<div class="card-body">
							<form:hidden path="menuPk" name="menuPk"  id="menuPk"/>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name"><span class="trn">Name</span></label>
										<div class="col-md-9">
											<form:input path="name" class="form-control ng-untouched ng-pristine ng-invalid"
											title="Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="name" id="name" name="name"
												placeholder="Enter Name" />
											<div class="ng-star-inserted">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name"><span class="trn">URL</span></label>
										<div class="col-md-9">
											<form:input path="url" class="form-control ng-untouched ng-pristine ng-invalid"
											title="URL" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="url" id="url" name="url"
												placeholder="Enter URL" />
											<div class="ng-star-inserted">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name"><span class="trn">Icon</span></label>
										<div class="col-md-9">
											<form:input path="icon" class="form-control ng-untouched ng-pristine ng-invalid"
											title="Icon" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="icon" id="icon" name="icon"
												placeholder="Enter Icon" />
											<div class="ng-star-inserted">
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>	
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive"><span class="trn">Has Child</span></label>
										<div class="col-md-9">
											<form:select path="child"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="child" id="child" name="child" onblur="validateCombo(this);" isrequired="true" title="Child">
												<form:option selected="true" disabled="true" value="-1">Please Select</form:option>
												<form:option value="1">Yes</form:option>
												<form:option value="0">No</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive"><span class="trn">IsActive</span></label>
										<div class="col-md-9">
											<form:select path="isActive"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="isactive" id="isactive" name="isactive" onblur="validateCombo(this);" isrequired="true" title="IsActive Type">
												<form:option selected="selected" value="-1">Select IsActive Type</form:option>
												<form:option value="0">0</form:option>
												<form:option value="1">1</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive"><span class="trn">Has Parent</span></label>
										<div class="col-md-3">
											<form:select path="parent" onchange="fnChkParentType()"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="parent" id="parent" name="parent" onblur="validateCombo(this);" isrequired="true" title="Parent">
												<form:option selected="true" disabled="true" value="-1">Please Select</form:option>
												<form:option value="1">Yes</form:option>
												<form:option value="0">No</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
										<div class="col-md-6" id="parentRecord" style="display: none;">
											<form:select path="parentSelfId"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="Self Parent" id="parentSelfId"
												name="parentSelfId" title="Self Parent">
												<form:option selected="true" disabled="true" value="-1">Please Select</form:option>
												<c:forEach items="${lstMainMenu}" var="entry">													
													<c:if test="${entry.child eq '1'}">
														<option value="${entry.menuPk}">${entry.name}</option>
													</c:if>
												</c:forEach>
											</form:select>

										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
						</div>
						
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveMenu()"
								disabled="">
								<i class="fa fa-dot-circle-o"></i> <span class="trn">Save</span>
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadMenu"></thead>								
										<tbody id="tbodyMenu"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/management/menuManage.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstMenu}';
prepareGrid(leadData);
</script>