<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Service Section</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Other Service </a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Other Service Information</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                   <div  class="row">
                                                      <div  class="col-md-12">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                            <div  class="col-md-9">
                                                               <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    <div  class="row">
										<div class="col-sm-12 table-responsive" id="divDatatable">
											<table aria-describedby="DataTables_Table_0_info"
												class="table table-striped table-bordered datatable dataTable no-footer "
												id="myTable" role="grid"
												style="border-collapse: collapse !important">
												<thead id="theadOtherService"></thead>								
												<tbody id="tbodyOtherService"></tbody>
											</table>
										</div>
									</div>
                                 </div>
                              </div>

                        
						</div>
                           <div id="create" class="tab-content" style="display:none">
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <div >
                                    <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                                       <div  class="row">
                                          <div  class="col-sm-12">
                                             <div  class="card">
                                                <div  class="card-header"><strong >Other Service Information</strong></div>
                                                <div  class="card-body">
                                                   <div  class="row">
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-12">
                                                         <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                            <tbody >
                                                               <tr >
                                                                  <th >Employee Code:</th>
                                                                  <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                                  <th ></th>
                                                                  <td  rowspan="4" style="width:150px;"><img  alt="admin@bootstrapmaster.com" height="150px" src="/resources/img/avatars/6.jpg" width="150px"></td>
                                                               </tr>
                                                               <tr >
                                                                  <th >Name:</th>
                                                                  <td ></td>
                                                                  <th >Rank:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Designation:</th>
                                                                  <td ></td>
                                                                  <th >Organisation:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Location:</th>
                                                                  <td ></td>
                                                                  <th >SPDB:</th>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="from_date">From Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="from_date" id="from_date" name="from_date" placeholder="Enter From Date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="to_date">To Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="to_date" id="to_date" name="to_date" placeholder="Enter To Date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="employer_name">Employer Name</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="employer_name" id="employer_name" name="employer_name" placeholder="Enter Employer Name" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="employer_address">Employer Address</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="employer_address" id="employer_address" name="employer_address" placeholder="Enter Employer Address" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="designation">Designation</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="designation" id="designation" name="designation" placeholder="Enter Designation" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="service_type">Service Type</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="service_type" id="service_type" name="service_type">
                                                                  <option  selected="selected" value="">Select Service Type</option>
                                                                  <!---->
                                                                  <option  value="58" class="ng-star-inserted">Teaching</option>
                                                                  <option  value="59" class="ng-star-inserted">autonomous</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Remarks</label>
                                                            <div  class="col-md-9">
                                                               <textarea  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="remarks" id="remarks" name="remarks" placeholder="Enter Remarks ....." rows="3"></textarea>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <input  formcontrolname="otherservice_id" id="otherservice_id" name="otherservice_id" type="hidden" value="0" class="ng-untouched ng-pristine ng-valid">
                                                <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="button" disabled=""><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/otherService.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstAward}';
prepareGrid(leadData);
</script>