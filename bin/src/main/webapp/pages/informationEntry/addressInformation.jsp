<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
<main class="main">
   <!-- Breadcrumb-->
   <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
      <li class="breadcrumb-item active">Address Information Section</li>
   </ol>
   <div class="container-fluid">
      <div  id="ui-view">
         <div >
            <div  class="animated fadeIn">
               <div  class="row">
                  <div  class="col-md-12 mb-12">
                     <ul  class="nav nav-tabs" role="tablist">
                        <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Address Information</a></li>
                        <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                     </ul>
                     <div  id="show" class="tab-content">
                        <div  class="tab-pane active" id="home" role="tabpanel">
                           <div  class="col-sm-12">
                              <div  class="card">
                                 <div  class="card-header"><strong >Address Information</strong></div>
                                 <div  class="card-body">
                                    <div  class="row">
                                       <div  class="col-sm-12">
                                          <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                             <div  class="row">
                                                <div  class="col-md-12">
                                                   <div  class="form-group row">
                                                      <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                      <div  class="col-md-9">
                                                         <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              <div  class="row">
                                 <div class="col-sm-12 table-responsive" id="divDatatable">
                                    <table aria-describedby="DataTables_Table_0_info"
                                       class="table table-striped table-bordered datatable dataTable no-footer "
                                       id="myTable" role="grid"
                                       style="border-collapse: collapse !important">
                                       <thead id="theadAddressInformation"></thead>
                                       <tbody id="tbodyAddressInformation"></tbody>
                                    </table>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                     <div  id="create" class="tab-content" style="display:none">
                        <div  class="tab-pane active" id="profile" role="tabpanel">
                           <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                              <div  class="row">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Address Information</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12 col-md-6"></div>
                                             <div  class="col-sm-12 col-md-6"></div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                   <tbody >
                                                      <tr >
                                                         <th >Employee Code:</th>
                                                         <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                         <th ></th>
                                                         <td  rowspan="4" style="width:150px;"><img src="/resources/img/avatars/6.jpg" alt="admin@bootstrapmaster.com" height="150px" width="150px"></td>
                                                      </tr>
                                                      <tr >
                                                         <th >Name:</th>
                                                         <td ></td>
                                                         <th >Rank:</th>
                                                      </tr>
                                                      <tr >
                                                         <th >Designation:</th>
                                                         <td ></td>
                                                         <th >Organisation:</th>
                                                      </tr>
                                                      <tr >
                                                         <th >Location:</th>
                                                         <td ></td>
                                                         <th >SPDB:</th>
                                                      </tr>
                                                   </tbody>
                                                </table>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <div  class="col-md-9"><strong >Permanent Address</strong></div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <div  class="col-md-9"><strong >Present Address</strong></div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <div  class="col-md-9"></div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <div  class="col-md-9">
                                                      <div  class="checkbox"><label ><input  type="checkbox" value=""> Same as permanent address</label></div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanenthouse">Village/House</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="permanenthouse" id="permanenthouse" name="permanenthouse" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presenthouse">Village/House</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presenthouse" id="presenthouse" name="presenthouse" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanenthousenative">Village/House(native)</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="permanenthousenative" id="permanenthousenative" name="permanenthousenative" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presenthousenative">Village/House(native)</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presenthousenative" id="presenthousenative" name="presenthousenative" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentblock">Road/Block/Sector</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="permanentblock" id="permanentblock" name="permanentblock" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentblock">Road/Block/Sector</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presentblock" id="presentblock" name="presentblock" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentblocknative">Road/Block/Sector(Native)</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="permanentblocknative" id="permanentblocknative" name="permanentblocknative" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentblocknative">Road/Block/Sector(Native)</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presentblocknative" id="presentblocknative" name="presentblocknative" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentpo">Post office</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-valid" formcontrolname="permanentpo" id="permanentpo" name="permanentpo">
                                                         <option  selected="selected" value="0">Select Post office</option>
                                                         <!---->
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentpo">Post office</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-valid" formcontrolname="presentpo" id="presentpo" name="poprpresentpoesent">
                                                         <option  selected="selected" value="0">Select Post office</option>
                                                         <!---->
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentps">Police Station</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-valid" formcontrolname="permanentps" id="permanentps" name="permanentps">
                                                         <option  selected="selected" value="0">Select Police Station</option>
                                                         <!---->
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presenttps">Police Station</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-valid" formcontrolname="presenttps" id="presenttps" name="presenttps">
                                                         <option  selected="selected" value="0">Select Police Station</option>
                                                         <!---->
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentdistrict">District</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-valid" formcontrolname="permanentdistrict" id="permanentdistrict" name="permanentdistrict">
                                                         <option  selected="selected" value="0">Select District</option>
                                                         <!---->
                                                         <option  value="2" class="ng-star-inserted">Manikganj</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentdistrict">District</label>
                                                   <div  class="col-md-9">
                                                      <select  class="form-control ng-untouched ng-pristine ng-valid" formcontrolname="presentdistrict" id="presentdistrict" name="presentdistrict">
                                                         <option  selected="selected" value="0">Select District</option>
                                                         <!---->
                                                         <option  value="2" class="ng-star-inserted">Manikganj</option>
                                                      </select>
                                                      <!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentphone">Phone</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="permanentphone" id="permanentphone" name="permanentphone" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentphone">Phone</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presentphone" id="presentphone" name="presentphone" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="permanentremarks">Remarks</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="permanentremarks" id="permanentremarks" name="permanentremarks" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentmobile">Mobile</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presentmobile" id="presentmobile" name="presentmobile" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <div  class="row">
                                             <div  class="col-sm-6"></div>
                                             <div  class="col-sm-6">
                                                <div  class="form-group row">
                                                   <label  class="col-md-3 col-form-label" for="presentremarks">Remarks</label>
                                                   <div  class="col-md-9">
                                                      <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="presentremarks" id="presentremarks" name="presentremarks" type="text"><!---->
                                                      <div  class="ng-star-inserted">
                                                         <!---->
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <input  formcontrolname="addressinfo_id" id="addressinfo_id" name="addressinfo_id" type="hidden" value="" class="ng-untouched ng-pristine ng-valid">
                                       <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="button" disabled=""><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                    </div>
                                 </div>
                              </div>
                           </form>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
      <div >
      </div>
   </div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/addressInformation.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
   var leadData = '${lstAddressInformation}';
   prepareGrid(leadData);
</script>