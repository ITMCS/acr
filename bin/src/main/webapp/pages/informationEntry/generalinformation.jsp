<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>

      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="../login.html">Home</a></li>
            <li class="breadcrumb-item active">General Information Component</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>General Information Details</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >General Information Section</strong></div>
                                    </div>
                                    <div  class="row">
                                       <div class="col-sm-12 table-responsive" id="divDatatable">
                                          <table aria-describedby="DataTables_Table_0_info"
                                             class="table table-striped table-bordered datatable dataTable no-footer "
                                             id="myTable" role="grid"
                                             style="border-collapse: collapse !important">
                                             <thead id="theadGeneralInformation"></thead>
                                             <tbody id="tbodyGeneralInformation"></tbody>
                                          </table>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div id="create" class="tab-content" style="display:none">
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                                    <div  class="row">
                                       <div  class="col-sm-12">
                                          <div  class="card">
                                             <div  class="card-header"><strong >General Information</strong></div>
                                             <div  class="card-body">
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_address">Govt ID</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_address" id="organisation_address" name="organisation_address" placeholder="Enter Govt ID" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_address">Photo</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_address" id="organisation_address" name="organisation_address" placeholder="Enter Orgainisation Address" type="file"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_address">Name(English)</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_address" id="organisation_address" name="organisation_address" placeholder="Enter Name(English)" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_mobile">Name(Bangla)</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_mobile" id="organisation_mobile" name="organisation_mobile" placeholder="Enter Name(Bangla)" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_name">Father's Name</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name" id="organisation_name" name="organisation_name" placeholder="Enter Father's Name" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_telephone">Father's Name(Bangla)</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_telephone" id="organisation_telephone" name="organisation_telephone" placeholder="Enter Father's Name(Bangla)" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_name_native">Mother's Name</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native" placeholder="Enter Mother's Name" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_email">Mother's Name(Bangla)</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_email" id="organisation_email" name="organisation_email" placeholder="Enter Mother's Name(Bangla)" type="text"><!---->
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="remarks">Date of Birth</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="remarks" id="remarks" name="remarks" placeholder="Enter Remarks" type="date">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Sex</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Sex</option>
                                                               <option  value="1" class="ng-star-inserted">Male</option>
                                                               <option  value="2" class="ng-star-inserted">Female</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Prefix Name</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Prefix Name</option>
                                                               <option  value="1" class="ng-star-inserted">Prefix 1</option>
                                                               <option  value="2" class="ng-star-inserted">Prefix 2</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Suffix Name</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Suffix Name</option>
                                                               <option  value="1" class="ng-star-inserted">Suffix 1</option>
                                                               <option  value="2" class="ng-star-inserted">Suffix 2</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Marital Status</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Marital Status</option>
                                                               <option  value="1" class="ng-star-inserted">married</option>
                                                               <option  value="2" class="ng-star-inserted">unmarried</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Home District</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Home District</option>
                                                               <option  value="1" class="ng-star-inserted">Ahmedabad</option>
                                                               <option  value="2" class="ng-star-inserted">Baroda</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Religion</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Religion</option>
                                                               <option  value="1" class="ng-star-inserted">Hindu</option>
                                                               <option  value="2" class="ng-star-inserted">Muslim</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Rank</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_name_native">Label</label>  
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_name_native">Designation</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_name_native">Label</label>  
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_email">location</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_name_native">Label</label>  
                                                            <div  class="ng-star-inserted">
                                                               <!---->
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_name_native">Organization</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_name_native">Label</label>  
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Order/Join Type</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Order/Join Type</option>
                                                               <option  value="1" class="ng-star-inserted">Join Type 1</option>
                                                               <option  value="2" class="ng-star-inserted">Join Type 2 </option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_name_native">O/J Date</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native" placeholder="Enter O/J Date" type="date"><!---->
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Freedom Fighter</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Freedom Fighter</option>
                                                               <option  value="1" class="ng-star-inserted">Yes</option>
                                                               <option  value="2" class="ng-star-inserted">No</option>
                                                            </select>
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native" value="if Yes" type="text" disabled>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_name_native">PRL Date</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_name_native">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">No. of Spouse</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_name_native">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Batch</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Batch</option>
                                                               <option  value="1" class="ng-star-inserted">Batch 1</option>
                                                               <option  value="2" class="ng-star-inserted">Batch 2</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Confirmation Go Date</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native"  type="date">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Confirmation Date</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native"  type="date">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Expire Date</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native"  type="date">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">National Seniority</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_type">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Status</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_type">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Promote</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Promote</option>
                                                               <option  value="1" class="ng-star-inserted">Promote 1</option>
                                                               <option  value="2" class="ng-star-inserted">Promote 2</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Cadre</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_type">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Cadre Date</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_type">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Activity Status</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Promote</option>
                                                               <option  value="1" class="ng-star-inserted">Promote 1</option>
                                                               <option  value="2" class="ng-star-inserted">Promote 2</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Placement</label>
                                                         <div  class="col-md-9">
                                                            <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_type" id="organisation_type" name="organisation_type">
                                                               <option  selected="selected" value="">Select Promote</option>
                                                               <option  value="1" class="ng-star-inserted">Promote 1</option>
                                                               <option  value="2" class="ng-star-inserted">Promote 2</option>
                                                            </select>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Code</label>
                                                         <div  class="col-md-9">
                                                            <label  class="col-md-3 col-form-label" for="organisation_type">Label</label>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="row">
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Additional Personal Info</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native" placeholder="Enter Additional Personal Info"  type="text">
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="col-sm-6">
                                                      <div  class="form-group row">
                                                         <label  class="col-md-3 col-form-label" for="organisation_type">Remarks</label>
                                                         <div  class="col-md-9">
                                                            <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="organisation_name_native" id="c" name="organisation_name_native" placeholder="Enter Remarks" type="text">
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </div>
                                          <input  formcontrolname="organisation_id" id="organisation_id" name="organisation_id" type="hidden" value="0" class="ng-untouched ng-pristine ng-valid">
                                          <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="button" disabled=""><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                       </div>
                                    </div>
                              </div>
                              </form>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/generalinformation.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstAward}';
prepareGrid(leadData);
</script>