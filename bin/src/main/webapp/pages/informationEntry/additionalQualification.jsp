<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
        document.getElementById("aNew").classList.remove("active");
        document.getElementById("aShow").classList.add("active");
        document.getElementById("show").style.display = "block";
        document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	document.getElementById("aShow").classList.remove("active");
        document.getElementById("aNew").classList.add("active");
        document.getElementById("show").style.display = "none";
        document.getElementById("create").style.display = "block";				
   }
   
</script>
<main class="main">
   <!-- Breadcrumb-->
   <ol class="breadcrumb">
      <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
      <li class="breadcrumb-item active"><span class="trn">Additional Qualification Section</span></li>
   </ol>
   <div class="container-fluid">
      <div  id="ui-view">
         <div >
            <div  class="animated fadeIn">
               <div  class="row">
                  <div  class="col-md-12 mb-12">
                     <ul  class="nav nav-tabs" role="tablist">
                        <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Additional Qualification</a></li>
                        <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                     </ul>
                     <div  id="show" class="tab-content">
                        <div  class="tab-pane active" id="home" role="tabpanel">
                           <div  class="col-sm-12">
                              <div  class="card">
                                 <div  class="card-header"><strong >Additional Qualification</strong></div>
                                 <div  class="card-body">
                                    <div  class="row">
                                       <div  class="col-sm-12">
                                          <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                             <div  class="row">
                                                <div  class="col-md-12">
                                                   <div  class="form-group row">
                                                      <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                      <div  class="col-md-9">
                                                         <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                      </div>
                                                   </div>
                                                </div>
                                             </div>
                                          </form>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                              
                            <div  class="row">
                                <div class="col-sm-12 table-responsive" id="divDatatable">
                                    <table aria-describedby="DataTables_Table_0_info"
                                        class="table table-striped table-bordered datatable dataTable no-footer "
                                        id="myTable" role="grid"
                                        style="border-collapse: collapse !important">
                                        <thead id="theadQualification"></thead>								
                                        <tbody id="tbodyQualification"></tbody>
                                    </table>
                                </div>
                            </div>	
                        </div>
                    </div>
               </div>
                     
                     
                     
                     <div id="create" class="tab-content" style="display:none">
                        <div  class="tab-pane active" id="profile" role="tabpanel">
                           <div >
                               <form:form role="form" name="qualificationform" action="${pageContext.servletContext.contextPath}/qualification/save" method="post" modelAttribute="modelQualification" > 
                                   <form:hidden path="qualificationPk" name="qualificationPk"  id="qualificationPk"/>
                                  
                                 <div  class="row">
                                    <div  class="col-sm-12">
                                       <div  class="card">
                                          <div  class="card-header"><strong >Additional Qualification</strong></div>
                                          <div  class="card-body">
                                             <div  class="row">
                                                <div  class="col-sm-12 col-md-6"></div>
                                                <div  class="col-sm-12 col-md-6"></div>
                                             </div>
                                             <div  class="row">
                                                <div  class="col-sm-12">
                                                   <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                      <tbody>
                                                         <tr>
                                                            <th >Employee Code:</th>
                                                            <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                            <th ></th>
                                                            <td  rowspan="4" style="width:150px;"><img src="/resources/img/avatars/default.png"  alt="admin@bootstrapmaster.com" height="150px"  width="150px"></td>
                                                         </tr>
                                                         <tr>
                                                            <th >Name:</th>
                                                            <td ></td>
                                                            <th >Rank:</th>
                                                         </tr>
                                                         <tr >
                                                            <th >Designation:</th>
                                                            <td ></td>
                                                            <th >Organisation:</th>
                                                         </tr>
                                                         <tr >
                                                            <th >Location:</th>
                                                            <td ></td>
                                                            <th >SPDB:</th>
                                                         </tr>
                                                      </tbody>
                                                   </table>
                                                </div>
                                             </div>
                                             <div  class="row" style="margin-top:10px;">
                                                <div  class="col-sm-6">
                                                   <div  class="form-group row">
                                                      <label  class="col-md-3 col-form-label" for="qualification_serial">Qualification Serial</label>
                                                      <div  class="col-md-9">
                                                          
                                                          <form:input path="qualificationSerial"
                                                        title="Qualification Serial" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                formcontrolname="qualification_serial" id="qualificationSerial"
                                                                name="qualificationSerial" placeholder="Enter Qualification Serial"
                                                                type="text"/>
                                                         <div  class="ng-star-inserted">
                                                            <!---->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="col-sm-6"></div>
                                             </div>
                                             <div  class="row">
                                                <div  class="col-sm-6">
                                                   <div  class="form-group row">
                                                      <label  class="col-md-3 col-form-label" for="additional_qualification">Additional Qualification</label>
                                                      <div  class="col-md-9">
                                                          <form:input path="additionalQualification"
                                                        title="Additional Qualification" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                formcontrolname="additional_qualification" id="additionalQualification"
                                                                name="additionalQualification" placeholder="Enter Additional Qualification"
                                                                type="text"/>
                                                         <div  class="ng-star-inserted">
                                                            <!---->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="col-sm-6">
                                                   <div  class="form-group"></div>
                                                </div>
                                             </div>
                                             <div  class="row">
                                                <div  class="col-sm-6">
                                                   <div  class="form-group row">
                                                      <label  class="col-md-3 col-form-label" for="remarks">Remarks</label>
                                                      <div  class="col-md-9">
                                                           <form:input path="remarks"
                                                        title="Remarks" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
                                                                class="form-control ng-untouched ng-pristine ng-invalid"
                                                                formcontrolname="remarks" id="remarks"
                                                                name="remarks" placeholder="Enter Remarks"
                                                                type="text"/>
                                                         <div  class="ng-star-inserted">
                                                            <!---->
                                                         </div>
                                                      </div>
                                                   </div>
                                                </div>
                                                <div  class="col-sm-6">
                                                   <div  class="form-group"></div>
                                                </div>
                                             </div>
                                          </div>
                                                         <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveQualification()" ><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                       </div>
                                    </div>
                                 </div>
                              </form:form>
                           </div>
                        </div>
                     </div>
</main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/additionalQualification.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var lstQualifications = '${lstQualifications}';
prepareGrid(lstQualifications);
</script>