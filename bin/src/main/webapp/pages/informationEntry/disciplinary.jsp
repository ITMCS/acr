<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Disciplinary Information Section</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Disciplinary</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="card-body">
                                    <div  class="col-sm-12">
                                       <div  class="card">
                                          <div  class="card-header"><strong >Disciplinary</strong></div>
                                          <div  class="card-body">
                                             <div  class="row">
                                                <div  class="col-sm-12">
                                                   <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                      <div  class="row">
                                                         <div  class="col-md-12">
                                                            <div  class="form-group row">
                                                               <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                               <div  class="col-md-9">
                                                                  <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </form>
                                                </div>
                                             </div>
                                          </div>
                                       </div>
                                       <div  class="row">
                                          <div class="col-sm-12 table-responsive" id="divDatatable">
                                             <table aria-describedby="DataTables_Table_0_info"
                                                class="table table-striped table-bordered datatable dataTable no-footer "
                                                id="myTable" role="grid"
                                                style="border-collapse: collapse !important">
                                                <thead id="theadDisciplinary"></thead>
                                                <tbody id="tbodyDisciplinary"></tbody>
                                             </table>
                                          </div>
                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div id="create" class="tab-content" style="display:none">
                              
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <div >
                                    <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                                       <div  class="row">
                                          <div  class="col-sm-12">
                                             <div  class="card">
                                                <div  class="card-header"><strong >Disciplinary</strong></div>
                                                <div  class="card-body">
                                                   <div  class="row">
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-12">
                                                         <table  aria-describedby="DataTables_Table_0_info" class="table table-striped table-bordered datatable dataTable no-footer" id="DataTables_Table_0" role="grid" style="border-collapse: collapse !important">
                                                            <tbody >
                                                               <tr >
                                                                  <th >Employee Code:</th>
                                                                  <td ><input  aria-controls="DataTables_Table_0" class="form-control form-control-sm" placeholder="Search..." type="search"></td>
                                                                  <th ></th>
                                                                  <td  rowspan="4" style="width:150px;"><img src="/resources/img/avatars/6.jpg" alt="admin@bootstrapmaster.com" height="150px" width="150px"></td>
                                                               </tr>
                                                               <tr >
                                                                  <th >Name:</th>
                                                                  <td ></td>
                                                                  <th >Rank:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Designation:</th>
                                                                  <td ></td>
                                                                  <th >Organisation:</th>
                                                               </tr>
                                                               <tr >
                                                                  <th >Location:</th>
                                                                  <td ></td>
                                                                  <th >SPDB:</th>
                                                               </tr>
                                                            </tbody>
                                                         </table>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="profilephoto">Go Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="profilephoto" id="profilephoto" name="profileimage" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="child_sex">Offence</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="child_sex" id="child_sex" name="child_sex">
                                                                  <option  selected="selected">Select Offence</option>
                                                                  <option >MISCONDUCT</option>
                                                                  <option >INEFFICIENCY</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="child_sex">Nature of Punishment</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="child_sex" id="child_sex" name="child_sex">
                                                                  <option  selected="selected">Select Nature of Punishment</option>
                                                                  <option >REDUC TO 2 LOWER STAGE IN SCAL</option>
                                                                  <option >STOPPAGE 2 INCREMENT</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="child_serial">Punishment Line 1</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="child_serial" id="child_serial" name="child_serial" placeholder="Enter Punishment Line 1" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="child_name">Punishment Line 2</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="child_name" id="child_name" name="child_name" placeholder="Enter Punishment Line 2" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group"></div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="date_of_birth">Remarks</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="date_of_birth" id="date_of_birth" name="date_of_birth" placeholder="Enter Remarks" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6"></div>
                                                   </div>
                                                </div>
                                                <input  formcontrolname="children_id" id="children_id" name="children_id" type="hidden" value="0" class="ng-untouched ng-pristine ng-valid">
                                                <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="submit"><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/disciplinary.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstAward}';
prepareGrid(leadData);
</script>