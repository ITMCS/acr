<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
   startLoading();
</script>
<style>
    
    .nav-tabs .nav-link.active, .nav-tabs .nav-item.show .nav-link {
    color: #5c6873;
    background-color: rgba(255, 255, 255, 0.9);
    border-color: #c8ced3 #c8ced3 rgba(255, 255, 255, 0.9);
}
</style>
<script type='text/javascript'>
   function hideShow()
   {
    	document.getElementById("aNew").classList.remove("active");
    	document.getElementById("aShow").classList.add("active");
    	document.getElementById("show").style.display = "block";
    	document.getElementById("create").style.display = "none";
   }
   function showHide()
   {
   	
   	document.getElementById("aShow").classList.remove("active");
   	document.getElementById("aNew").classList.add("active");
   	document.getElementById("show").style.display = "none";
   	document.getElementById("create").style.display = "block";				
   }
   
</script>
      <main class="main">
         <!-- Breadcrumb-->
         <ol class="breadcrumb">
            <li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index">Home</a></li>
            <li class="breadcrumb-item active">Promotion Information</li>
         </ol>
         <div class="container-fluid">
            <div  id="ui-view">
               <div >
                  <div  class="animated fadeIn">
                     <div  class="row">
                        <div  class="col-md-12 mb-12">
                           <ul  class="nav nav-tabs" role="tablist">
                              <li  class="nav-item"><a  id="aShow" aria-controls="home" class="nav-link active" data-toggle="tab" role="tab" onclick='hideShow()'>Promotion</a></li>
                              <li  class="nav-item"><a  id="aNew"aria-controls="profile" class="nav-link" data-toggle="tab" role="tab" onclick='showHide()'>Create New</a></li>
                           </ul>
                           <div id="show" class="tab-content">
                              <div  class="tab-pane active" id="home" role="tabpanel">
                                 <div  class="col-sm-12">
                                    <div  class="card">
                                       <div  class="card-header"><strong >Promotion</strong></div>
                                       <div  class="card-body">
                                          <div  class="row">
                                             <div  class="col-sm-12">
                                                <form  action="" class="form-horizontal ng-untouched ng-pristine ng-valid" method="post" novalidate="">
                                                   <div  class="row">
                                                      <div  class="col-md-12">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Search By Employee Code</label>
                                                            <div  class="col-md-9">
                                                               <div  class="input-group"><input  class="form-control" id="input2-group2" name="input2-group2" placeholder="Search By Employee Code" type="email"><span  class="input-group-append"><button  class="btn btn-primary" type="button"><i  class="fa fa-search"></i> Search</button></span></div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                </form>
                                             </div>
                                          </div>
                                       </div>
                                    </div>
                                    
									 <div  class="row">
										<div class="col-sm-12 table-responsive" id="divDatatable">
											<table aria-describedby="DataTables_Table_0_info"
												class="table table-striped table-bordered datatable dataTable no-footer "
												id="myTable" role="grid"
												style="border-collapse: collapse !important">
												<thead id="theadPromotion"></thead>								
												<tbody id="tbodyPromotion"></tbody>
											</table>
										</div>
									</div>	
									
                                 </div>
                              </div>
                          

						  </div>


                           <div id="create" class="tab-content" style="display:none">
                              <div  class="tab-pane active" id="profile" role="tabpanel">
                                 <div >
                                    <form  class="form-horizontal ng-untouched ng-pristine ng-invalid" method="post" novalidate="">
                                       <div  class="row">
                                          <div  class="col-sm-12">
                                             <div  class="card">
                                                <div  class="card-header"><strong >Promotion</strong></div>
                                                <div  class="card-body">
                                                   <div  class="row">
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                      <div  class="col-sm-12 col-md-6"></div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="natureofpromotion">Nature of Promotion</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="natureofpromotion" id="natureofpromotion" name="natureofpromotion">
                                                                  <option  selected="selected">Select Nature of Promotion</option>
                                                                  <!---->
                                                                  <option  value="62" class="ng-star-inserted">FIRST CLASS</option>
                                                                  <option  value="63" class="ng-star-inserted">SECON CLASS</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="promotiondate">Promotion Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="promotiondate" id="promotiondate" name="promotiondate" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="godate">GO Date</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="godate" id="godate" name="godate" placeholder="date" type="date"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="payscale">Pay Scale</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="payscale" id="payscale" name="payscale">
                                                                  <option  selected="selected">Select Pay Scale</option>
                                                                  <!---->
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="rank">Rank</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="rank" id="rank" name="rank">
                                                                  <option  selected="selected">Select Rank</option>
                                                                  <!---->
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="rule">Rule</label>
                                                            <div  class="col-md-9">
                                                               <select  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="rule" id="rule" name="rule">
                                                                  <option  selected="selected">Select Rule</option>
                                                                  <!---->
                                                                  <option  value="64" class="ng-star-inserted">275</option>
                                                                  <option  value="65" class="ng-star-inserted">380</option>
                                                               </select>
                                                               <!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="remarks">Remarks</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="remarks" id="remarks" name="remarks" placeholder="Enter Remarks" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="details">Details</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="details" id="details" name="details" placeholder="Enter Details" type="text"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                   </div>
                                                   <div  class="row">
                                                      <div  class="col-sm-6">
                                                         <div  class="form-group row">
                                                            <label  class="col-md-3 col-form-label" for="profileimage">Image</label>
                                                            <div  class="col-md-9">
                                                               <input  class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="profileimage" id="profileimage" name="profileimage" type="file"><!---->
                                                               <div  class="ng-star-inserted">
                                                                  <!---->
                                                               </div>
                                                            </div>
                                                         </div>
                                                      </div>
                                                      <div  class="col-sm-6"></div>
                                                   </div>
                                                </div>
                                                <input  formcontrolname="promotion_id" id="promotion_id" name="promotion_id" type="hidden" value="0" class="ng-untouched ng-pristine ng-valid">
                                                <div  class="card-footer"><button  class="btn btn-sm btn-success float-right" type="button"><i  class="fa fa-dot-circle-o"></i> Save </button></div>
                                             </div>
                                          </div>
                                       </div>
                                    </form>
                                 </div>
                              </div>
                           </div>
                        
                        </div>
                     </div>
                  </div>
               </div>
            </div>
            <div ></div>
         </div>
      </main>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/informationEntry/promotion.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstAward}';
prepareGrid(leadData);
</script>