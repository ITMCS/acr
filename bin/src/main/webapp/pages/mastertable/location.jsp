<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
startLoading();
</script>
<main class="main">
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a
		href="${pageContext.servletContext.contextPath}/index">Home</a></li>
	<li class="breadcrumb-item active">Location Component</li>
</ol>
<div class="container-fluid">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-sm-12">
				<div class="card">
					<div class="card-header">
						<strong>Location</strong>
					</div>
					<form:form role="form" name="locationform"
						id="locationform"
						onsubmit="return false;" method="post"
						modelAttribute="modellocation">
						<div class="card-body">

							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name">Name</label>
										<div class="col-md-9">
											<form:input path="locationName"
											title="Location Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="name" id="locationName"
												name="locationName" placeholder="Enter Location Name"  />
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name_native">Name
											Native</label>
										<div class="col-md-9">
											<form:input path="locationNativeName"
												class="form-control ng-untouched ng-pristine ng-invalid"
												title="Location Native Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="name_native" id="name_native"
												name="name_native" placeholder="Enter Location Name Native"
												type="text" />
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							<div  class="row">
				                  <div  class="col-sm-8">
				                     <div  class="form-group row">
				                        <label  class="col-md-3 col-form-label" for="name">type</label>
				                        <div  class="col-md-9">
				                           <form:input  path="locationType"  
				                           title="Location Type" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
				                           class="form-control ng-untouched ng-pristine ng-invalid" formcontrolname="type" id="type" name="type" placeholder="Enter Location Type"/><!---->
				                           <div  class="ng-star-inserted">
				                           </div>
				                        </div>
				                     </div>
				                  </div>
				                  <div  class="col-sm-4"></div>
				            </div>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive">IsActive</label>
										<div class="col-md-9">
											<form:select path="isActive"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="isactive" id="isactive" name="isactive" onblur="validateCombo(this);" isrequired="true" title="IsActive Type">
												<form:option selected="selected" value="-1">Select IsActive Type</form:option>
												<form:option value="0">0</form:option>
												<form:option value="1">1</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
						</div>
						<form:hidden formcontrolname="location_id" id="location_id"
							name="locationPk" value="" path="locationPk"
							class="ng-untouched ng-pristine ng-valid"/>
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right"
								type="button" onclick="fnSaveLocation()" disabled="">
								<i class="fa fa-dot-circle-o"></i> Save
							</form:button>
						</div>
					</form:form>
					<div class="card-body" id="locationdatadiv">
						<div class="row"></div>
						<div class="row">
							<div class="col-sm-12 table-responsive">
								<table aria-describedby="DataTables_Table_0_info"
									class="table table-striped table-bordered datatable dataTable no-footer "
									id="myTable" role="grid"
									style="border-collapse: collapse !important">
									<thead>
										<tr class="text-center" role="row">
											<th aria-controls="DataTables_Table_0"
												aria-label="Date registered: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1"
												style="width: 150px;" tabindex="0">Id</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Role: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1"
												style="width: 230px;" tabindex="0">Name</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Status: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1" style="width: 20%;"
												tabindex="0">Name Native</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Status: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1" style="width: 20%;"
												tabindex="0">Keyword</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Status: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1" style="width: 30%;"
												tabindex="0">Abbreviation</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Status: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1" style="width: 30%;"
												tabindex="0">Abbreviation Native</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Status: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1" style="width: 30%;"
												tabindex="0">Parent Id</th>
											<th aria-controls="DataTables_Table_0"
												aria-label="Actions: activate to sort column ascending"
												class="sorting" colspan="1" rowspan="1"
												style="width: 180px;" tabindex="0">Action</th>
										</tr>
									</thead>
									<tbody id="tbodyLocations">
										<c:set var="count" value="1" />
										<c:forEach items="${lstLocations}" var="entry">
											<tr class="text-center">
												<td>${count}</td>
												<td>${entry.locationName}</td>
												<td>${entry.locationNativeName}</td>
												<td>-</td>
												<td>-</td>
												<td>-</td>
												<td>-</td>
												<td>
												<button class="btn btn-info" type="button" module="location" value="${entry.locationPk}" onclick="fnEdit(this)" ><b><i class="fa fa-edit"></i></b></button>
												<button class="btn btn-danger" type="button" module="location" value="${entry.locationPk}" onclick="fnDelete(this)" ><b><i class="fa fa-trash-o"></i></b></button>
												</td>
											</tr>
											<c:set var="count" value="${count+1}" />
										</c:forEach>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
</main>

<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/mastertable/location.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
$(document).ready( function () {
	//$('#myTable').DataTable().fnDestroy();
   $('#myTable').DataTable({
		 	 "lengthMenu": [[5,10, 25, 50, -1], [5,10, 25, 50, "All"]]
		 });
    hideLoading();
    
});

</script>
