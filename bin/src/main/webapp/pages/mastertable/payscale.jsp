<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
startLoading();
</script>



<main class="main"> <!-- Breadcrumb-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">PayScale Component</span></li>
</ol>




<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">Pims PayScale</span></strong>
						</div>
						<form:form role="form" name="PayScaleform" action="${pageContext.servletContext.contextPath}/payscale/save" onsubmit="return false;" method="post" modelAttribute="modelPayScale" >
						<div class="card-body">
							<form:hidden path="payScalePk" name="payScalePk"  id="payScalePk"/>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name"><span class="trn">PayScale</span></label>
										<div class="col-md-9">
											<form:input path="scale" class="form-control ng-untouched ng-pristine ng-invalid"
											title="scale" validarr="required@@numeric" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="scale" id="scale" name="scale"
												placeholder="Enter Scale" type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name_native"><span class="trn">Grade</span></label>
										<div class="col-md-9">
											<form:input path="grade"
											title="Grade" validarr="required@@" tovalid="true" onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="grade" id="grade"
												name="grade" placeholder="Enter grade"
												type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div  class="row">
							  <div  class="col-sm-8">
								 <div  class="form-group row">
									<label  class="col-md-3 col-form-label" for="keyword"><span class="trn">PayScale Year</span></label>
									<div  class="col-md-9">
									  <form:input path="year"
											title="year" validarr="required@@numeric" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="year" id="year"
												name="year" placeholder="Enter PayScale Year"
												type="text"/>
									   <div  class="ng-star-inserted">
										  <!---->
									   </div>
									</div>
								 </div>
							  </div>
							  <div  class="col-sm-4"></div>
						   </div>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive"><span class="trn">IsActive</span></label>
										<div class="col-md-9">
											<form:select path="isActive"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="isactive" id="isactive" name="isactive" onblur="validateCombo(this);" isrequired="true" title="IsActive Type">
												<form:option selected="selected" value="-1">Select IsActive Type</form:option>
												<form:option value="0">0</form:option>
												<form:option value="1">1</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
							
						</div>
						
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSavePayScale()"
								disabled="">
								<i class="fa fa-dot-circle-o"></i><span class="trn"> Save</span>
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadpayscale"></thead>								
										<tbody id="tbodypayscale"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>

<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/mastertable/PayScale.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstPayScale}';
prepareGrid(leadData);
</script>