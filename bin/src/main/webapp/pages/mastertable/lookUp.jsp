<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/common.js"></script>
<script>
startLoading();
</script>

<main class="main"> <!-- Breadcrumb-->
<ol class="breadcrumb">
	<li class="breadcrumb-item"><a href="${pageContext.servletContext.contextPath}/index"><span class="trn">Home</span></a></li>
	<li class="breadcrumb-item active"><span class="trn">Main Lookup Component</span></li>
</ol>


<div class="container-fluid">
	<div class="animated fadeIn">
			<div class="row">
				<div class="col-sm-12">
					<div class="card">
						<div class="card-header">
							<strong><span class="trn">Main Lookup Management Section</span></strong>
						</div>
						<form:form role="form" name="lookupform" action="${pageContext.servletContext.contextPath}/lookup/save" onsubmit="return false;" method="post" modelAttribute="modelLookUp" >
						<div class="card-body">
							<form:hidden path="lookUpPk" name="lookUpPk"  id="lookUpPk"/>
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name"><span class="trn">Name</span></label>
										<div class="col-md-9">
											<form:input path="lookUpName" class="form-control ng-untouched ng-pristine ng-invalid"
											title="Lookup Name" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												formcontrolname="name" id="lookUpName" name="lookUpName"
												placeholder="Enter Name" type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="name_native"><span class="trn">Name Native</span></label>
										<div class="col-md-9">
											<form:input path="lookUpNativeName"
											title="LookUp Name Native" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="name_native" id="name_native"
												name="name_native" placeholder="Enter Name Native"
												type="text"/>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>

								</div>
								<div class="col-sm-4"></div>
							</div>
							
							<div  class="row">
							  <div  class="col-sm-8">
								 <div  class="form-group row">
									<label  class="col-md-3 col-form-label" for="keyword"><span class="trn">Keyword</span></label>
									<div  class="col-md-9">
									  <form:input path="keyword"
											title="Keyword" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="keyword" id="keyword"
												name="keyword" placeholder="Enter Keyword"
												type="text"/>
									   <div  class="ng-star-inserted">
										  <!---->
									   </div>
									</div>
								 </div>
							  </div>
							  <div  class="col-sm-4"></div>
						   </div>
							
							<div  class="row">
								  <div  class="col-sm-8">
									 <div  class="form-group row">
										<label  class="col-md-3 col-form-label" for="abbreviation"><span class="trn">Abbreviation</span></label>
										<div  class="col-md-9">
										   <form:input path="abbreviation"
											title="Abbreviation" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="abbreviation" id="abbreviation"
												name="abbreviation" placeholder="Enter Abbreviation"
												type="text"/>
										   <div  class="ng-star-inserted">
											  <!---->
										   </div>
										</div>
									 </div>
								  </div>
								  <div  class="col-sm-4"></div>
						   </div>
							
						   <div  class="row">
							  <div  class="col-sm-8">
								 <div  class="form-group row">
									<label  class="col-md-3 col-form-label" for="abbreviation_native"><span class="trn">Abbreviation Native</span></label>
									<div  class="col-md-9">
									  <form:input path="abbreviationNative"
											title="Abbreviation Native" validarr="required@@" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="abbreviationNative" id="abbreviationNative"
												name="abbreviationNative" placeholder="Enter Abbreviation Native"
												type="text"/>
												
									   <div  class="ng-star-inserted">
										  <!---->
									   </div>
									</div>
								 </div>
							  </div>
							  <div  class="col-sm-4"></div>
						   </div>							
							
			               <div  class="row">
									  <div  class="col-sm-8">
										 <div  class="form-group row">
											<label  class="col-md-3 col-form-label" for="siNo"><span class="trn">SI No</span></label>
											<div  class="col-md-9">
										<form:input path="siNo"
											title="SI No" validarr="required@@numeric" tovalid="true"  onblur="validateTextComponent(this)"
												class="form-control ng-untouched ng-pristine ng-invalid"
												formcontrolname="siNo" id="siNo"
												name="siNo" placeholder="Enter SI No"
												type="text"/>
											   <div  class="ng-star-inserted">
												  <!---->
											   </div>
											</div>
										 </div>
									  </div>
									  <div  class="col-sm-4"></div>
								   </div>
							
							
							<div class="row">
								<div class="col-sm-8">
									<div class="form-group row">
										<label class="col-md-3 col-form-label" for="isactive"><span class="trn">IsActive</span></label>
										<div class="col-md-9">
											<form:select path="isActive"
												class="form-control ng-untouched ng-pristine ng-valid"
												formcontrolname="isactive" id="isactive" name="isactive" onblur="validateCombo(this);" isrequired="true" title="IsActive Type">
												<form:option selected="selected" value="-1">Select IsActive Type</form:option>
												<form:option value="0">0</form:option>
												<form:option value="1">1</form:option>
											</form:select>
											<!---->
											<div class="ng-star-inserted">
												<!---->
											</div>
										</div>
									</div>
								</div>
								<div class="col-sm-4"></div>
							</div>
							
							
						</div>
						
						<div class="card-footer" style="height: 52px;">
							<form:button class="btn btn-sm btn-success float-right" type="button" onclick="fnSaveLookUp()"
								disabled="">
								<i class="fa fa-dot-circle-o"></i> <span class="trn">Save</span>
							</form:button>
						</div>
						</form:form>
						<div class="card-body">
							<div class="row"></div>
							<div class="row">
								<div class="col-sm-12 table-responsive" id="divDatatable">
									<table aria-describedby="DataTables_Table_0_info"
										class="table table-striped table-bordered datatable dataTable no-footer "
										id="myTable" role="grid"
										style="border-collapse: collapse !important">
										<thead id="theadLookUp"></thead>								
										<tbody id="tbodyLookUp"></tbody>
									</table>
								</div>
							</div>							
						</div>
					</div>
				</div>
			</div>
	</div>
</div>
</main>

<script	type="text/javascript" charset="utf8" src="${pageContext.servletContext.contextPath}/resources/customJS/mastertable/lookUp.js"></script>
<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
<script>
var leadData = '${lstLookUp}';
prepareGrid(leadData);
</script>
